<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {


	function __construct(){
		parent::__construct();		
		$this->load->model('Login_model');
		$this->load->model('Tbl_kontak_model');
		

		if($this->session->userdata('status') != "login"){
			redirect(base_url("login"));
		}


	} 

	public function index()
	{
		$q = urldecode($this->input->get('q', TRUE));
		$start = intval($this->input->get('start'));

		if ($q <> '') {
			$config['base_url'] = base_url() . 'admin/?q=' . urlencode($q);
			$config['first_url'] = base_url() . 'admin/?q=' . urlencode($q);
		} else {
			$config['base_url'] = base_url() . 'admin/';
			$config['first_url'] = base_url() . 'admin/';
		}

		$config['per_page'] = 10;
		$config['page_query_string'] = TRUE;
		$config['total_rows'] = $this->Tbl_kontak_model->total_rows($q);
		$tbl_kontak = $this->Tbl_kontak_model->get_limit_data($config['per_page'], $start, $q);

		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$data = array(
			'tbl_kontak_data' => $tbl_kontak,
			'q' => $q,
			'pagination' => $this->pagination->create_links(),
			'total_rows' => $config['total_rows'],
			'start' => $start,
		);
		renderPage('admin/index',$data);

	}
}
