<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Tbl_news_model');
		$this->load->model('Tbl_gallery_model');

		
		$this->load->library('form_validation');

	}

	public function index()
	{
		$q = urldecode($this->input->get('q', TRUE));
		$start = intval($this->input->get('start'));

		if ($q <> '') {
			$config['base_url'] = base_url() . 'blog/?q=' . urlencode($q);
			$config['first_url'] = base_url() . 'blog/?q=' . urlencode($q);
		} else {
			$config['base_url'] = base_url() . 'blog/';
			$config['first_url'] = base_url() . 'blog/';
		}

		$config['per_page'] = 10;
		$config['page_query_string'] = TRUE;
		$config['total_rows'] = $this->Tbl_news_model->total_rows($q);
		$tbl_news = $this->Tbl_news_model->get_limit_data($config['per_page'], $start, $q);

		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$tbl_gallery = $this->Tbl_gallery_model->get_limit_8();


		$data = array(
			'tbl_gallery_data' => $tbl_gallery,

			'tbl_news_data' => $tbl_news,
			'q' => $q,
			'pagination' => $this->pagination->create_links(),
			'total_rows' => $config['total_rows'],
			'start' => $start,



			'img_path'      => './assets/captcha/',
			'img_url'       => base_url('assets/captcha'),
			'font_path' 	=> './assets/font/timesbd.ttf',	
			'word_length'   => 4,
			'font_size'     => 10,	
			'img_width'     => 100,
			'img_height'    => 19,
			'expiration'    => 7200,


			'action' => site_url('welcome/kontak_action'),
			'id_kontak' => set_value('id_kontak'),
			'nama' => set_value('nama'),
			'email' => set_value('email'),
			'hp' => set_value('hp'),
			'pesan' => set_value('pesan')

		);


		$captcha = create_captcha($data);

		$sessionCaptcha = $this->session->unset_userdata('captchacode');
		$sessionCaptcha = $this->session->set_userdata('captchacode', $captcha['word']);

		$data['cap_img'] = $captcha['image'];

		
		$this->load->view('blog', $data);

	}


	public function read($id) 
	{
		$row = $this->Tbl_news_model->get_by_id($id);

		$tbl_news = $this->Tbl_news_model->get_limit_3();

		if ($row) {
			$data = array(
				'tbl_news_data' => $tbl_news,

				'id_news' => $row->id_news,
				'judul' => $row->judul,
				'desk' => $row->desk,
				'foto' => $row->foto,
				'konten' => $row->konten,
				'tgl_input' => $row->tgl_input,


				'img_path'      => './assets/captcha/',
				'img_url'       => base_url('assets/captcha'),
				'font_path' 	=> './assets/font/timesbd.ttf',	
				'word_length'   => 4,
				'font_size'     => 10,	
				'img_width'     => 100,
				'img_height'    => 19,
				'expiration'    => 7200,


				'action' => site_url('welcome/kontak_action'),
				'id_kontak' => set_value('id_kontak'),
				'nama' => set_value('nama'),
				'email' => set_value('email'),
				'hp' => set_value('hp'),
				'pesan' => set_value('pesan')
			);


			$captcha = create_captcha($data);

			$sessionCaptcha = $this->session->unset_userdata('captchacode');
			$sessionCaptcha = $this->session->set_userdata('captchacode', $captcha['word']);

			$data['cap_img'] = $captcha['image'];
			$this->load->view('read_blog', $data);
		} else {
			$this->session->set_flashdata('message', 'Blog tidak di temukan.');
			redirect(site_url('blog'));
		}
	}
}
