<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Tbl_gallery_model');
		$this->load->library('form_validation');

	}

	public function index()
	{
		$q = urldecode($this->input->get('q', TRUE));
		$start = intval($this->input->get('start'));

		if ($q <> '') {
			$config['base_url'] = base_url() . 'gallery/?q=' . urlencode($q);
			$config['first_url'] = base_url() . 'gallery/?q=' . urlencode($q);
		} else {
			$config['base_url'] = base_url() . 'gallery/';
			$config['first_url'] = base_url() . 'gallery/';
		}

		$config['per_page'] = 16;
		$config['page_query_string'] = TRUE;
		$config['total_rows'] = $this->Tbl_gallery_model->total_rows($q);
		$tbl_gallery = $this->Tbl_gallery_model->get_limit_data($config['per_page'], $start, $q);

		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$data = array(
			'tbl_gallery_data' => $tbl_gallery,
			'q' => $q,
			'pagination' => $this->pagination->create_links(),
			'total_rows' => $config['total_rows'],
			'start' => $start,

			'img_path'      => './assets/captcha/',
			'img_url'       => base_url('assets/captcha'),
			'font_path' 	=> './assets/font/timesbd.ttf',	
			'word_length'   => 4,
			'font_size'     => 10,	
			'img_width'     => 100,
			'img_height'    => 19,
			'expiration'    => 7200,


			'action' => site_url('welcome/kontak_action'),
			'id_kontak' => set_value('id_kontak'),
			'nama' => set_value('nama'),
			'email' => set_value('email'),
			'hp' => set_value('hp'),
			'pesan' => set_value('pesan')


		);

		$captcha = create_captcha($data);

		$sessionCaptcha = $this->session->unset_userdata('captchacode');
		$sessionCaptcha = $this->session->set_userdata('captchacode', $captcha['word']);

		$data['cap_img'] = $captcha['image'];
		
		$this->load->view('gallery', $data);

	}


}
