<?php
class Login extends CI_Controller{
	
	function __construct(){
		parent::__construct();		
		$this->load->model('Login_model');
		
	}
	
	function index(){
		$this->load->view('login/index.php');
	}
	
	function aksi_login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$where = array(
			'email' => $username,
			//'pass' => md5($password)
			'pass' => $password
		);
		$cek = $this->Login_model->cek_login("tbl_admin",$where)->num_rows();
		if($cek > 0){

			$_SESSION['KCFINDER']=array();
			$_SESSION['KCFINDER']['disabled'] = false;
			$_SESSION['KCFINDER']['uploadURL'] = '';
			$_SESSION['KCFINDER']['uploadDir'] = '';  


			$data_session = array(
				'email' => $username,
				'status' => "login",
			);

			$this->session->set_flashdata('disabled', 'false');

			
			
			$this->session->set_userdata($data_session);
			
			redirect(base_url("admin"));
			
		}else{
			echo "Username dan password salah !";
			redirect(base_url("login"));
		}
		
	}
	
	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('login'));
	}
}