<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tbl_admin extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tbl_admin_model');
        $this->load->library('form_validation');

        if($this->session->userdata('status') != "login"){
            redirect(base_url("login"));
        }
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'tbl_admin/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'tbl_admin/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'tbl_admin/index.html';
            $config['first_url'] = base_url() . 'tbl_admin/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Tbl_admin_model->total_rows($q);
        $tbl_admin = $this->Tbl_admin_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tbl_admin_data' => $tbl_admin,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        renderPage('tbl_admin/tbl_admin_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Tbl_admin_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_user' => $row->id_user,
		'email' => $row->email,
		'pass' => $row->pass,
	    );
            $this->load->view('tbl_admin/tbl_admin_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tbl_admin'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('tbl_admin/create_action'),
	    'id_user' => set_value('id_user'),
	    'email' => set_value('email'),
	    'pass' => set_value('pass'),
	);
        renderPage('tbl_admin/tbl_admin_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'email' => $this->input->post('email',TRUE),
		'pass' => $this->input->post('pass',TRUE),
	    );

            $this->Tbl_admin_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('tbl_admin'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Tbl_admin_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('tbl_admin/update_action'),
		'id_user' => set_value('id_user', $row->id_user),
		'email' => set_value('email', $row->email),
		'pass' => set_value('pass', $row->pass),
	    );
            $this->load->view('tbl_admin/tbl_admin_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tbl_admin'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_user', TRUE));
        } else {
            $data = array(
		'email' => $this->input->post('email',TRUE),
		'pass' => $this->input->post('pass',TRUE),
	    );

            $this->Tbl_admin_model->update($this->input->post('id_user', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('tbl_admin'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Tbl_admin_model->get_by_id($id);

        if ($row) {
            $this->Tbl_admin_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('tbl_admin'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tbl_admin'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('email', 'email', 'trim|required');
	$this->form_validation->set_rules('pass', 'pass', 'trim|required');

	$this->form_validation->set_rules('id_user', 'id_user', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Tbl_admin.php */
/* Location: ./application/controllers/Tbl_admin.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-12-02 10:29:14 */
/* http://harviacode.com */