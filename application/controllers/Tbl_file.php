<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tbl_file extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tbl_file_model');
         $this->load->model('Tbl_folder_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'tbl_file/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'tbl_file/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'tbl_file/index.html';
            $config['first_url'] = base_url() . 'tbl_file/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Tbl_file_model->total_rows($q);
        $tbl_file = $this->Tbl_file_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tbl_file_data' => $tbl_file,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $data['tbl_folder_data'] = $this->Tbl_folder_model->get_all();

         renderPage('tbl_file/tbl_file_list',$data);

        //$this->load->view('tbl_file/tbl_file_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Tbl_file_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_file' => $row->id_file,
		'nama_file' => $row->nama_file,
		'tgl_input' => $row->tgl_input,
	    );
            $this->load->view('tbl_file/tbl_file_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tbl_file'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('tbl_file/create_action'),
	    'id_file' => set_value('id_file'),
	    'nama_file' => set_value('nama_file'),
	    'tgl_input' => set_value('tgl_input'),
	);
         renderPage('tbl_file/tbl_file_form',$data);
       // $this->load->view('tbl_file/tbl_file_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama_file' => $this->input->post('nama_file',TRUE),
		'tgl_input' => $this->input->post('tgl_input',TRUE),
	    );

            $this->Tbl_file_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('tbl_file'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Tbl_file_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('tbl_file/update_action'),
		'id_file' => set_value('id_file', $row->id_file),
		'nama_file' => set_value('nama_file', $row->nama_file),
		'tgl_input' => set_value('tgl_input', $row->tgl_input),
	    );
            $this->load->view('tbl_file/tbl_file_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tbl_file'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_file', TRUE));
        } else {
            $data = array(
		'nama_file' => $this->input->post('nama_file',TRUE),
		'tgl_input' => $this->input->post('tgl_input',TRUE),
	    );

            $this->Tbl_file_model->update($this->input->post('id_file', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('tbl_file'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Tbl_file_model->get_by_id($id);

        if ($row) {
            $this->Tbl_file_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('tbl_file'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tbl_file'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama_file', 'nama file', 'trim|required');
	$this->form_validation->set_rules('tgl_input', 'tgl input', 'trim|required');

	$this->form_validation->set_rules('id_file', 'id_file', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Tbl_file.php */
/* Location: ./application/controllers/Tbl_file.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-02-10 18:45:51 */
/* http://harviacode.com */