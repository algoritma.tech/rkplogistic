<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tbl_folder extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tbl_folder_model');
        $this->load->library('form_validation');
        
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'tbl_folder/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'tbl_folder/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'tbl_folder/index.html';
            $config['first_url'] = base_url() . 'tbl_folder/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Tbl_folder_model->total_rows($q);
        $tbl_folder = $this->Tbl_folder_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tbl_folder_data' => $tbl_folder,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
         renderPage('tbl_folder/tbl_folder_list',$data);
       // $this->load->view('tbl_folder/tbl_folder_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Tbl_folder_model->get_by_id($id);
        if ($row) {
            $data = array(
              'id_folder' => $row->id_folder,
              'nama_folder' => $row->nama_folder,
              'tgl_input' => $row->tgl_input,
          );
            $this->load->view('tbl_folder/tbl_folder_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tbl_folder'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('tbl_folder/create_action'),
            'id_folder' => set_value('id_folder'),
            'nama_folder' => set_value('nama_folder'),
            'tgl_input' => set_value('tgl_input'),
        );


 renderPage('tbl_folder/tbl_folder_form',$data);
        //$this->load->view('tbl_folder/tbl_folder_form', $data);
    }
    
    public function create_action() 
    {


        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
              'nama_folder' => $this->input->post('nama_folder',TRUE),
              'tgl_input' => $this->input->post('tgl_input',TRUE),
          );

// echo $data['nama_folder'];
// exit();
            $data['nama_folder'] = str_replace( ':', '', $data['nama_folder']);
            if (!is_dir('assets/images/'.$data['nama_folder'])) {
                mkdir('./assets/images/' . $data['nama_folder'], 0777, TRUE);

            }


            $this->Tbl_folder_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('tbl_folder'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Tbl_folder_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('tbl_folder/update_action'),
                'id_folder' => set_value('id_folder', $row->id_folder),
                'nama_folder' => set_value('nama_folder', $row->nama_folder),
                'tgl_input' => set_value('tgl_input', $row->tgl_input),
            );
            $this->load->view('tbl_folder/tbl_folder_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tbl_folder'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_folder', TRUE));
        } else {
            $data = array(
              'nama_folder' => $this->input->post('nama_folder',TRUE),
              'tgl_input' => $this->input->post('tgl_input',TRUE),
          );

            $this->Tbl_folder_model->update($this->input->post('id_folder', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('tbl_folder'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Tbl_folder_model->get_by_id($id);

        if ($row) {
            $data = array(
              'nama_folder' => $row->nama_folder, 
          );
           // $data['nama_folder'] = str_replace( ':', '', $data['nama_folder']);
          //rmdir('./assets/images/' . $data['nama_folder'], 0777, TRUE);
       //     echo  $data['nama_folder'] ;

        //rmdir('./assets/images/' . $data['nama_folder'], 0777, TRUE);

            $path='./assets/images/'.$data['nama_folder'];
        $this->load->helper("file"); // load the helper
        rmdir($path);
       // echo $path;

        //exit();


        $this->Tbl_folder_model->delete($id);
        $this->session->set_flashdata('message', 'Delete Record Success');
        redirect(site_url('tbl_folder'));
    } else {
        $this->session->set_flashdata('message', 'Record Not Found');
        redirect(site_url('tbl_folder'));
    }
}

public function _rules() 
{
   $this->form_validation->set_rules('nama_folder', 'nama folder', 'trim|required');
   $this->form_validation->set_rules('tgl_input', 'tgl input', 'trim|required');

   $this->form_validation->set_rules('id_folder', 'id_folder', 'trim');
   $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
}

}

/* End of file Tbl_folder.php */
/* Location: ./application/controllers/Tbl_folder.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-02-10 19:16:25 */
/* http://harviacode.com */