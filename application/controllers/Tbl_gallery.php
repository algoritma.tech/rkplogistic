<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Tbl_gallery extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tbl_gallery_model');
        $this->load->library('form_validation');

        if($this->session->userdata('status') != "login"){
            redirect(base_url("login"));
        }
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'tbl_gallery/?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'tbl_gallery/?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'tbl_gallery/';
            $config['first_url'] = base_url() . 'tbl_gallery/';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Tbl_gallery_model->total_rows($q);
        $tbl_gallery = $this->Tbl_gallery_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tbl_gallery_data' => $tbl_gallery,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        renderPage('tbl_gallery/tbl_gallery_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Tbl_gallery_model->get_by_id($id);
        if ($row) {
            $data = array(
              'id_gallery' => $row->id_gallery,
              'nama_gallery' => $row->nama_gallery,
              'foto' => $row->foto,
              'desk' => $row->desk,
		      // 'tgl_input' => $row->tgl_input,
          );
            $this->load->view('tbl_gallery/tbl_gallery_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tbl_gallery'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('tbl_gallery/create_action'),
            'id_gallery' => set_value('id_gallery'),
            'nama_gallery' => set_value('nama_gallery'),
            'foto' => set_value('foto'),
            'desk' => set_value('desk'),
        );
        renderPage('tbl_gallery/tbl_gallery_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {

            $config['upload_path']='./assets/images/gallery';
            $config['allowed_types']='gif|jpg|jpeg|png';
            $config['max_size']='2048';
            $config['max_width']='1500';
            $config['max_height']='1500';
            $this->upload->initialize($config);

            if($this->upload->do_upload('foto')){
                $upload_data=$this->upload->data();
                $nama_file=$upload_data['file_name'];

                $data = array(
                  'nama_gallery' => $this->input->post('nama_gallery',TRUE),
                  'foto' => $nama_file,
                  'desk' => $this->input->post('desk',TRUE),
              );

                $this->Tbl_gallery_model->insert($data);
                $this->session->set_flashdata('message', 'Create Record Success');
                redirect(site_url('tbl_gallery'));
            } else {

               $data = array(
                'button' => 'Create',
                'action' => site_url('tbl_gallery/create_action'),
                'id_gallery' => set_value('id_gallery'),
                'nama_gallery' => set_value('nama_gallery'),
                'foto' => set_value('foto'),
                'desk' => set_value('desk'),
            );
               $this->session->set_flashdata('message', 'Error'.$this->upload->display_errors());

               renderPage('tbl_gallery/tbl_gallery_form', $data);

           }
       }
   }

   public function update($id) 
   {
    $row = $this->Tbl_gallery_model->get_by_id($id);

    if ($row) {
        $data = array(
            'button' => 'Update',
            'action' => site_url('tbl_gallery/update_action'),
            'id_gallery' => set_value('id_gallery', $row->id_gallery),
            'nama_gallery' => set_value('nama_gallery', $row->nama_gallery),
            'foto' => set_value('foto', $row->foto),
            'desk' => set_value('desk', $row->desk),
        );
        renderPage('tbl_gallery/tbl_gallery_form', $data);
    } else {
        $this->session->set_flashdata('message', 'Record Not Found');
        redirect(site_url('tbl_gallery'));
    }
}

public function update_action() 
{
    $this->_rules();

    if ($this->form_validation->run() == FALSE) {
        $this->update($this->input->post('id_gallery', TRUE));
    } else {

    
        $data = array(
          'nama_gallery' => $this->input->post('nama_gallery',TRUE),
          'desk' => $this->input->post('desk',TRUE),
      );

        $this->Tbl_gallery_model->update($this->input->post('id_gallery', TRUE), $data);
        $this->session->set_flashdata('message', 'Update Record Success');
        redirect(site_url('tbl_gallery'));
        
    }
}

public function delete($id) 
{
    $row = $this->Tbl_gallery_model->get_by_id($id);

    if ($row) {
        $this->Tbl_gallery_model->delete($id);
        $this->session->set_flashdata('message', 'Delete Record Success');
        redirect(site_url('tbl_gallery'));
    } else {
        $this->session->set_flashdata('message', 'Record Not Found');
        redirect(site_url('tbl_gallery'));
    }
}

public function _rules() 
{
 $this->form_validation->set_rules('nama_gallery', 'nama gallery', 'trim|required');
	   // $this->form_validation->set_rules('foto', 'foto', 'trim|required');
 $this->form_validation->set_rules('desk', 'desk', 'trim|required');
	   // $this->form_validation->set_rules('tgl_input', 'tgl input', 'trim|required');

 $this->form_validation->set_rules('id_gallery', 'id_gallery', 'trim');
 $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
}

}

/* End of file Tbl_gallery.php */
/* Location: ./application/controllers/Tbl_gallery.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-01-08 09:47:11 */
/* http://harviacode.com */