<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Tbl_konten extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tbl_konten_model');
        $this->load->library('form_validation');

        if($this->session->userdata('status') != "login"){
            redirect(base_url("login"));
        }
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'tbl_konten/?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'tbl_konten/?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'tbl_konten/';
            $config['first_url'] = base_url() . 'tbl_konten/';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Tbl_konten_model->total_rows($q);
        $tbl_konten = $this->Tbl_konten_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tbl_konten_data' => $tbl_konten,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        renderPage('tbl_konten/tbl_konten_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Tbl_konten_model->get_by_id($id);
        if ($row) {
            $data = array(
              'id_konten' => $row->id_konten,
              'nama_konten' => $row->nama_konten,
              'desk' => $row->desk,
              'konten' => $row->konten,
              'foto' => $row->foto,
              'tgl_input' => $row->tgl_input,
          );
            $this->load->view('tbl_konten/tbl_konten_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tbl_konten'));
        }
    }

    public function update_foto($id) 
    {
        $row = $this->Tbl_konten_model->get_by_id($id);
        if ($row) {
            $data = array(
                'button' => 'Upload Foto',
                'action' => site_url('tbl_konten/create_action'),

                'id_konten' => $row->id_konten,
                'nama_konten' => $row->nama_konten,
                'desk' => $row->desk,
                'konten' => $row->konten,
                'foto' => $row->foto,
                'tgl_input' => $row->tgl_input,
            );
            renderPage('tbl_konten/update_foto', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tbl_konten'));
        }
    }

    



    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('tbl_konten/create_action'),
            'id_konten' => set_value('id_konten'),
            'nama_konten' => set_value('nama_konten'),
            'desk' => set_value('desk'),
            'konten' => set_value('konten'),
            'foto' => set_value('foto'),
        );
        renderPage('tbl_konten/tbl_konten_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {

            $config['upload_path']='./assets/images/konten';
            $config['allowed_types']='gif|jpg|jpeg|png';
            $config['max_size']='2048';
            $config['max_width']='1500';
            $config['max_height']='1500';
            $this->upload->initialize($config);

            if($this->upload->do_upload('foto')){
                $upload_data=$this->upload->data();
                $nama_file=$upload_data['file_name'];

                $data = array(
                  'nama_konten' => $this->input->post('nama_konten',TRUE),
                  'desk' => $this->input->post('desk',TRUE),
                  'konten' => $this->input->post('konten',TRUE),
                  'foto' => $nama_file,
                  
              );

                $this->Tbl_konten_model->insert($data);
                $this->session->set_flashdata('message', 'Create Record Success');
                redirect(site_url('tbl_konten'));
            } else {

                $data = array(
                    'button' => 'Create',
                    'action' => site_url('tbl_konten/create_action'),
                    'id_konten' => set_value('id_konten'),
                    'nama_konten' => set_value('nama_konten'),
                    'desk' => set_value('desk'),
                    'konten' => set_value('konten'),
                    'foto' => set_value('foto'),
                );
                $this->session->set_flashdata('Error','error foto' );
                renderPage('tbl_konten/tbl_konten_form', $data);

            }
        }
    }

    public function update($id) 
    {
        $row = $this->Tbl_konten_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('tbl_konten/update_action'),
                'id_konten' => set_value('id_konten', $row->id_konten),
                'nama_konten' => set_value('nama_konten', $row->nama_konten),
                'desk' => set_value('desk', $row->desk),
                'konten' => set_value('konten', $row->konten),
                'foto' => set_value('foto', $row->foto),
                // 'tgl_input' => set_value('tgl_input', $row->tgl_input),
            );
            renderPage('tbl_konten/tbl_konten_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tbl_konten'));
        }
    }

    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_konten', TRUE));
        } else {

            $config['upload_path']='./assets/images/konten';
            $config['allowed_types']='gif|jpg|jpeg|png';
            $config['max_size']='2048';
            $config['max_width']='1500';
            $config['max_height']='1500';
            $this->upload->initialize($config);

            if($this->upload->do_upload('foto')){
                $upload_data=$this->upload->data();
                $nama_file=$upload_data['file_name'];

                $data = array(
                  'nama_konten' => $this->input->post('nama_konten',TRUE),
                  'desk' => $this->input->post('desk',TRUE),
                  'konten' => $this->input->post('konten',TRUE),
                  'foto' => $nama_file,
                  // 'tgl_input' => $this->input->post('tgl_input',TRUE),
              );

                $this->Tbl_konten_model->update($this->input->post('id_konten', TRUE), $data);
                $this->session->set_flashdata('message', 'Update Record Success');
                redirect(site_url('tbl_konten'));
            }
        }
    }

    public function delete($id) 
    {
        $row = $this->Tbl_konten_model->get_by_id($id);

        if ($row) {
            $this->Tbl_konten_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('tbl_konten'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tbl_konten'));
        }
    }

    public function _rules() 
    {
       $this->form_validation->set_rules('nama_konten', 'nama konten', 'trim|required');
       $this->form_validation->set_rules('desk', 'desk', 'trim|required');
       $this->form_validation->set_rules('konten', 'konten', 'trim|required');
       // $this->form_validation->set_rules('foto', 'foto', 'trim|required');
       // $this->form_validation->set_rules('tgl_input', 'tgl input', 'trim|required');

       $this->form_validation->set_rules('id_konten', 'id_konten', 'trim');
       $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
   }

}

/* End of file Tbl_konten.php */
/* Location: ./application/controllers/Tbl_konten.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-01-09 05:57:20 */
/* http://harviacode.com */