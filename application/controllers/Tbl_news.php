<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Tbl_news extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tbl_news_model');
        $this->load->library('form_validation');

        if($this->session->userdata('status') != "login"){
            redirect(base_url("login"));
        }
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'tbl_news/?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'tbl_news/?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'tbl_news/';
            $config['first_url'] = base_url() . 'tbl_news/';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Tbl_news_model->total_rows($q);
        $tbl_news = $this->Tbl_news_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tbl_news_data' => $tbl_news,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        renderPage('tbl_news/tbl_news_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Tbl_news_model->get_by_id($id);
        if ($row) {
            $data = array(
              'id_news' => $row->id_news,
              'judul' => $row->judul,
              'desk' => $row->desk,
              'foto' => $row->foto,
              'konten' => $row->konten,
              // 'tgl_input' => $row->tgl_input,
          );
            $this->load->view('tbl_news/tbl_news_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tbl_news'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('tbl_news/create_action'),
            'id_news' => set_value('id_news'),
            'judul' => set_value('judul'),
            'desk' => set_value('desk'),
            'foto' => set_value('foto'),
            'konten' => set_value('konten'),
            // 'tgl_input' => set_value('tgl_input'),
        );
        renderPage('tbl_news/tbl_news_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {

            $config['upload_path']='./assets/images/news';
            $config['allowed_types']='gif|jpg|jpeg|png';
            $config['max_size']='2048';
            $config['max_width']='2000';
            $config['max_height']='2000';
            $this->upload->initialize($config);

            if($this->upload->do_upload('foto')){
                $upload_data=$this->upload->data();
                $nama_file=$upload_data['file_name'];

                $data = array(
                  'judul' => $this->input->post('judul',TRUE),
                  'desk' => $this->input->post('desk',TRUE),
                  'foto' => $nama_file,
                  'konten' => $this->input->post('konten',TRUE),
                  // 'tgl_input' => $this->input->post('tgl_input',TRUE),
              );

                $this->Tbl_news_model->insert($data);
                $this->session->set_flashdata('message', 'Create Record Success');
                redirect(site_url('tbl_news'));
            } else {

                $data = array(
                    'button' => 'Create',
                    'action' => site_url('tbl_news/create_action'),
                    'id_news' => set_value('id_news'),
                    'judul' => set_value('judul'),
                    'desk' => set_value('desk'),
                    'foto' => set_value('foto'),
                    'konten' => set_value('konten'),

                );

                $this->session->set_flashdata('message', 'Error images'.$this->upload->display_errors());
                renderPage('tbl_news/tbl_news_form', $data);

            }
        }
    }
    
    public function update($id) 
    {
        $row = $this->Tbl_news_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('tbl_news/update_action'),
                'id_news' => set_value('id_news', $row->id_news),
                'judul' => set_value('judul', $row->judul),
                'desk' => set_value('desk', $row->desk),
                'foto' => set_value('foto', $row->foto),
                'konten' => set_value('konten', $row->konten),
            );
            renderPage('tbl_news/tbl_news_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tbl_news'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_news', TRUE));
        } else {

            $data = array(
              'judul' => $this->input->post('judul',TRUE),
              'desk' => $this->input->post('desk',TRUE),
              'konten' => $this->input->post('konten',TRUE),
          );

            $this->Tbl_news_model->update($this->input->post('id_news', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('tbl_news'));
        }
    }
    

    public function delete($id) 
    {
        $row = $this->Tbl_news_model->get_by_id($id);

        if ($row) {
            $this->Tbl_news_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('tbl_news'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tbl_news'));
        }
    }

    public function _rules() 
    {
       $this->form_validation->set_rules('judul', 'judul', 'trim|required');
       $this->form_validation->set_rules('desk', 'desk', 'trim|required');
     // $this->form_validation->set_rules('foto', 'foto', 'trim|required');
       $this->form_validation->set_rules('konten', 'konten', 'trim|required');
     // $this->form_validation->set_rules('tgl_input', 'tgl input', 'trim|required');

       $this->form_validation->set_rules('id_news', 'id_news', 'trim');
       $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
   }

}

/* End of file Tbl_news.php */
/* Location: ./application/controllers/Tbl_news.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-01-08 10:52:07 */
/* http://harviacode.com */