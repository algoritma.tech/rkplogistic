<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tbl_produk extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tbl_produk_model');
        $this->load->library('form_validation');

        if($this->session->userdata('status') != "login"){
            redirect(base_url("login"));
        }
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'tbl_produk/?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'tbl_produk/?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'tbl_produk/';
            $config['first_url'] = base_url() . 'tbl_produk/';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Tbl_produk_model->total_rows($q);
        $tbl_produk = $this->Tbl_produk_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tbl_produk_data' => $tbl_produk,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        renderPage('tbl_produk/tbl_produk_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Tbl_produk_model->get_by_id($id);
        if ($row) {
            $data = array(
              'id_produk' => $row->id_produk,
              'id_kategori_produk' => $row->id_kategori_produk,
              'nama_produk' => $row->nama_produk,
              'foto1' => $row->foto1,
              'foto2' => $row->foto2,
              'foto3' => $row->foto3,
              'desk' => $row->desk,
              'harga' => $row->harga,
              'berat' => $row->berat,
              'task' => $row->task,
              // 'tgl_input' => $row->tgl_input,
          );
            $this->load->view('tbl_produk/tbl_produk_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tbl_produk'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('tbl_produk/create_action'),
            'id_produk' => set_value('id_produk'),
            'id_kategori_produk' => set_value('id_kategori_produk'),
            'nama_produk' => set_value('nama_produk'),
            'foto1' => set_value('foto1'),
            'foto2' => set_value('foto2'),
            'foto3' => set_value('foto3'),
            'desk' => set_value('desk'),
            'harga' => set_value('harga'),
            'berat' => set_value('berat'),
            'task' => set_value('task'),
            // 'tgl_input' => set_value('tgl_input'),
        );
        renderPage('tbl_produk/tbl_produk_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {

            $config['upload_path']='./assets/images/produk';
            $config['allowed_types']='gif|jpg|jpeg|png';
            $config['max_size']='2048';
            $config['max_width']='1500';
            $config['max_height']='1500';
            $this->upload->initialize($config);

            if($this->upload->do_upload('foto1','foto2','foto3')){
                $upload_data=$this->upload->data();
                $nama_file=$upload_data['file_name'];

                $data = array(
                  'id_kategori_produk' => $this->input->post('id_kategori_produk',TRUE),
                  'nama_produk' => $this->input->post('nama_produk',TRUE),
                  'foto1' => $nama_file,
                  'foto2' => $nama_file,
                  'foto3' => $nama_file,
                  'desk' => $this->input->post('desk',TRUE),
                  'harga' => $this->input->post('harga',TRUE),
                  'berat' => $this->input->post('berat',TRUE),
                  'task' => $this->input->post('task',TRUE),
                  // 'tgl_input' => $this->input->post('tgl_input',TRUE),
              );

                $this->Tbl_produk_model->insert($data);
                $this->session->set_flashdata('message', 'Create Record Success');
                redirect(site_url('tbl_produk'));
            }
        }
    }
    
    public function update($id) 
    {
        $row = $this->Tbl_produk_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('tbl_produk/update_action'),
                'id_produk' => set_value('id_produk', $row->id_produk),
                'id_kategori_produk' => set_value('id_kategori_produk', $row->id_kategori_produk),
                'nama_produk' => set_value('nama_produk', $row->nama_produk),
                'foto1' => set_value('foto1', $row->foto1),
                'foto2' => set_value('foto2', $row->foto2),
                'foto3' => set_value('foto3', $row->foto3),
                'desk' => set_value('desk', $row->desk),
                'harga' => set_value('harga', $row->harga),
                'berat' => set_value('berat', $row->berat),
                'task' => set_value('task', $row->task),
                // 'tgl_input' => set_value('tgl_input', $row->tgl_input),
            );
            renderPage('tbl_produk/tbl_produk_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tbl_produk'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_produk', TRUE));
        } else {

            $config['upload_path']='./assets/images/produk';
            $config['allowed_types']='gif|jpg|jpeg|png';
            $config['max_size']='2048';
            $config['max_width']='1500';
            $config['max_height']='1500';
            $this->upload->initialize($config);

            if($this->upload->do_upload('foto1','foto2','foto3')){
                $upload_data=$this->upload->data();
                $nama_file=$upload_data['file_name'];

                $data = array(
                  'id_kategori_produk' => $this->input->post('id_kategori_produk',TRUE),
                  'nama_produk' => $this->input->post('nama_produk',TRUE),
                  'foto1' => $nama_file,
                  'foto2' => $nama_file,
                  'foto3' => $nama_file,
                  'desk' => $this->input->post('desk',TRUE),
                  'harga' => $this->input->post('harga',TRUE),
                  'berat' => $this->input->post('berat',TRUE),
                  'task' => $this->input->post('task',TRUE),
                  // 'tgl_input' => $this->input->post('tgl_input',TRUE),
              );

                $this->Tbl_produk_model->update($this->input->post('id_produk', TRUE), $data);
                $this->session->set_flashdata('message', 'Update Record Success');
                redirect(site_url('tbl_produk'));
            }
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Tbl_produk_model->get_by_id($id);

        if ($row) {
            $this->Tbl_produk_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('tbl_produk'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tbl_produk'));
        }
    }

    public function _rules() 
    {
     $this->form_validation->set_rules('id_kategori_produk', 'id kategori produk', 'trim|required');
     $this->form_validation->set_rules('nama_produk', 'nama produk', 'trim|required');
     // $this->form_validation->set_rules('foto1', 'foto1', 'trim|required');
     // $this->form_validation->set_rules('foto2', 'foto2', 'trim|required');
     // $this->form_validation->set_rules('foto3', 'foto3', 'trim|required');
     $this->form_validation->set_rules('desk', 'desk', 'trim|required');
     $this->form_validation->set_rules('harga', 'harga', 'trim|required');
     $this->form_validation->set_rules('berat', 'berat', 'trim|required');
     $this->form_validation->set_rules('task', 'task', 'trim|required');
     // $this->form_validation->set_rules('tgl_input', 'tgl input', 'trim|required');

     $this->form_validation->set_rules('id_produk', 'id_produk', 'trim');
     $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
 }

}

/* End of file Tbl_produk.php */
/* Location: ./application/controllers/Tbl_produk.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-01-10 06:49:15 */
/* http://harviacode.com */