<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Tbl_profile extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->model('Tbl_profile_model');
    $this->load->library('form_validation');

    if($this->session->userdata('status') != "login"){
      redirect(base_url("login"));
    }
  }

  public function index()
  {
    $q = urldecode($this->input->get('q', TRUE));
    $start = intval($this->input->get('start'));

    if ($q <> '') {
      $config['base_url'] = base_url() . 'tbl_profile/?q=' . urlencode($q);
      $config['first_url'] = base_url() . 'tbl_profile/?q=' . urlencode($q);
    } else {
      $config['base_url'] = base_url() . 'tbl_profile/';
      $config['first_url'] = base_url() . 'tbl_profile/';
    }

    $config['per_page'] = 10;
    $config['page_query_string'] = TRUE;
    $config['total_rows'] = $this->Tbl_profile_model->total_rows($q);
    $tbl_profile = $this->Tbl_profile_model->get_limit_data($config['per_page'], $start, $q);

    $this->load->library('pagination');
    $this->pagination->initialize($config);

    $data = array(
      'tbl_profile_data' => $tbl_profile,
      'q' => $q,
      'pagination' => $this->pagination->create_links(),
      'total_rows' => $config['total_rows'],
      'start' => $start,
    );
    renderPage('tbl_profile/tbl_profile_list', $data);
  }

  public function read($id) 
  {
    $row = $this->Tbl_profile_model->get_by_id($id);
    if ($row) {
      $data = array(
        'id_profile' => $row->id_profile,
        'nama_usaha' => $row->nama_usaha,
        'logo' => $row->logo,
        'alamat' => $row->alamat,
        'maps' => $row->maps,
        'url_fb' => $row->url_fb,
        'url_ig' => $row->url_ig,
        'url_tw' => $row->url_tw,
        'no_telp' => $row->no_telp,
        'no_hp' => $row->no_hp,
        'no_wa' => $row->no_wa,
      );
      $this->load->view('tbl_profile/tbl_profile_read', $data);
    } else {
      $this->session->set_flashdata('message', 'Record Not Found');
      redirect(site_url('tbl_profile'));
    }
  }

  public function create() 
  {
    $data = array(
      'button' => 'Create',
      'action' => site_url('tbl_profile/create_action'),
      'id_profile' => set_value('id_profile'),
      'nama_usaha' => set_value('nama_usaha'),
      'logo' => set_value('logo'),
      'alamat' => set_value('alamat'),
      'maps' => set_value('maps'),
      'url_fb' => set_value('url_fb'),
      'url_ig' => set_value('url_ig'),
      'url_tw' => set_value('url_tw'),
      'no_telp' => set_value('no_telp'),
      'no_hp' => set_value('no_hp'),
      'no_wa' => set_value('no_wa'),
    );
    renderPage('tbl_profile/tbl_profile_form', $data);
  }

  public function create_action() 
  {
    $this->_rules();

    if ($this->form_validation->run() == FALSE) {
      $this->create();
    } else {

      $config['upload_path']='./assets/images/profile';
      $config['allowed_types']='gif|jpg|jpeg|png';
      $config['max_size']='2048';
      $config['max_width']='1500';
      $config['max_height']='1500';
      $this->upload->initialize($config);

      if($this->upload->do_upload('logo')){
        $upload_data=$this->upload->data();
        $nama_file=$upload_data['file_name'];

        $data = array(
          'nama_usaha' => $this->input->post('nama_usaha',TRUE),
          'logo' => $nama_file,
          'alamat' => $this->input->post('alamat',TRUE),
          'maps' => $this->input->post('maps',TRUE),
          'url_fb' => $this->input->post('url_fb',TRUE),
          'url_ig' => $this->input->post('url_ig',TRUE),
          'url_tw' => $this->input->post('url_tw',TRUE),
          'no_telp' => $this->input->post('no_telp',TRUE),
          'no_hp' => $this->input->post('no_hp',TRUE),
          'no_wa' => $this->input->post('no_wa',TRUE),
        );

        $this->Tbl_profile_model->insert($data);
        $this->session->set_flashdata('message', 'Create Record Success');
        redirect(site_url('tbl_profile'));
      } else {

        $data = array(
          'button' => 'Create',
          'action' => site_url('tbl_profile/create_action'),
          'id_profile' => set_value('id_profile'),
          'nama_usaha' => set_value('nama_usaha'),
          'logo' => set_value('logo'),
          'alamat' => set_value('alamat'),
          'maps' => set_value('maps'),
          'url_fb' => set_value('url_fb'),
          'url_ig' => set_value('url_ig'),
          'url_tw' => set_value('url_tw'),
          'no_telp' => set_value('no_telp'),
          'no_hp' => set_value('no_hp'),
          'no_wa' => set_value('no_wa'),
        );

        $this->session->set_flashdata('message', 'Error'.$this->upload->display_errors());


        renderPage('tbl_profile/tbl_profile_form', $data);
      }
    }
  }

  public function update($id) 
  {
    $row = $this->Tbl_profile_model->get_by_id($id);

    if ($row) {
      $data = array(
        'button' => 'Update',
        'action' => site_url('tbl_profile/update_action'),
        'id_profile' => set_value('id_profile', $row->id_profile),
        'nama_usaha' => set_value('nama_usaha', $row->nama_usaha),
        'alamat' => set_value('alamat', $row->alamat),
        'maps' => set_value('maps', $row->maps),
        'url_fb' => set_value('url_fb', $row->url_fb),
        'url_ig' => set_value('url_ig', $row->url_ig),
        'url_tw' => set_value('url_tw', $row->url_tw),
        'no_telp' => set_value('no_telp', $row->no_telp),
        'no_hp' => set_value('no_hp', $row->no_hp),
        'no_wa' => set_value('no_wa', $row->no_wa),
      );
      renderPage('tbl_profile/tbl_profile_form', $data);
    } else {
      $this->session->set_flashdata('message', 'Record Not Found');
      redirect(site_url('tbl_profile'));
    }
  }

  public function update_action() 
  {
    $this->_rules();

    if ($this->form_validation->run() == FALSE) {
      $this->update($this->input->post('id_profile', TRUE));
    } else {


      $data = array(
        'nama_usaha' => $this->input->post('nama_usaha',TRUE),
        'alamat' => $this->input->post('alamat',TRUE),
        'maps' => $this->input->post('maps',TRUE),
        'url_fb' => $this->input->post('url_fb',TRUE),
        'url_ig' => $this->input->post('url_ig',TRUE),
        'url_tw' => $this->input->post('url_tw',TRUE),
        'no_telp' => $this->input->post('no_telp',TRUE),
        'no_hp' => $this->input->post('no_hp',TRUE),
        'no_wa' => $this->input->post('no_wa',TRUE),
      );

      $this->Tbl_profile_model->update($this->input->post('id_profile', TRUE), $data);
      $this->session->set_flashdata('message', 'Update Record Success');
      redirect(site_url('tbl_profile'));
      
    }
  }

  public function delete($id) 
  {
    $row = $this->Tbl_profile_model->get_by_id($id);

    if ($row) {
      $this->Tbl_profile_model->delete($id);
      $this->session->set_flashdata('message', 'Delete Record Success');
      redirect(site_url('tbl_profile'));
    } else {
      $this->session->set_flashdata('message', 'Record Not Found');
      redirect(site_url('tbl_profile'));
    }
  }

  public function _rules() 
  {
   $this->form_validation->set_rules('nama_usaha', 'nama usaha', 'trim|required');
	// $this->form_validation->set_rules('logo', 'logo', 'trim|required');
   $this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
   $this->form_validation->set_rules('maps', 'maps', 'trim|required');
   $this->form_validation->set_rules('url_fb', 'url fb', 'trim|required');
   $this->form_validation->set_rules('url_ig', 'url ig', 'trim|required');
   $this->form_validation->set_rules('url_tw', 'url tw', 'trim|required');
   $this->form_validation->set_rules('no_telp', 'no telp', 'trim|required');
   $this->form_validation->set_rules('no_hp', 'no hp', 'trim|required');
   $this->form_validation->set_rules('no_wa', 'no wa', 'trim|required');

   $this->form_validation->set_rules('id_profile', 'id_profile', 'trim');
   $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
 }

}

/* End of file Tbl_profile.php */
/* Location: ./application/controllers/Tbl_profile.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-01-09 07:41:24 */
/* http://harviacode.com */