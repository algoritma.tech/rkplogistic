<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tbl_slidder extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tbl_slidder_model');
        $this->load->library('form_validation');

        if($this->session->userdata('status') != "login"){
            redirect(base_url("login"));
        }
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'tbl_slidder/?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'tbl_slidder/?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'tbl_slidder/';
            $config['first_url'] = base_url() . 'tbl_slidder/';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Tbl_slidder_model->total_rows($q);
        $tbl_slidder = $this->Tbl_slidder_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tbl_slidder_data' => $tbl_slidder,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        renderPage('tbl_slidder/tbl_slidder_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Tbl_slidder_model->get_by_id($id);
        if ($row) {
            $data = array(
              'id_slidder' => $row->id_slidder,
              'nama' => $row->nama,
              'desk' => $row->desk,
              'foto' => $row->foto,
              'status' => $row->status,
              'tgl_input' => $row->tgl_input,
          );
            $this->load->view('tbl_slidder/tbl_slidder_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tbl_slidder'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('tbl_slidder/create_action'),
            'id_slidder' => set_value('id_slidder'),
            'nama' => set_value('nama'),
            'desk' => set_value('desk'),
            'foto' => set_value('foto'),
            'status' => set_value('status'),
            'tgl_input' => set_value('tgl_input'),
        );
        renderPage('tbl_slidder/tbl_slidder_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {

            $config['upload_path']='./assets/images/slidder';
            $config['allowed_types']='gif|jpg|jpeg|png';
            $config['max_size']='2048';
            $config['max_width']='1500';
            $config['max_height']='1500';
            $this->upload->initialize($config);

            if($this->upload->do_upload('foto')){
                $upload_data=$this->upload->data();
                $nama_file=$upload_data['file_name'];

                $data = array(
                  'nama' => $this->input->post('nama',TRUE),
                  'desk' => $this->input->post('desk',TRUE),
                  'foto' => $nama_file,
                  'status' => $this->input->post('status',TRUE),
                  'tgl_input' => $this->input->post('tgl_input',TRUE),
              );

                $this->Tbl_slidder_model->insert($data);
                $this->session->set_flashdata('message', 'Create Record Success');
                redirect(site_url('tbl_slidder'));
            } else {


             $data = array(
                'button' => 'Create',
                'action' => site_url('tbl_slidder/create_action'),
                'id_slidder' => set_value('id_slidder'),
                'nama' => set_value('nama'),
                'desk' => set_value('desk'),
                'foto' => set_value('foto'),
                'status' => set_value('status'),
                'tgl_input' => set_value('tgl_input'),
            );
             $this->session->set_flashdata('message', '<div class="alert alert-danger">Gagal Upload Foto</div>');

             renderPage('tbl_slidder/tbl_slidder_form', $data);

         }
     }
 }

 public function update($id) 
 {
    $row = $this->Tbl_slidder_model->get_by_id($id);

    if ($row) {
        $data = array(
            'button' => 'Update',
            'action' => site_url('tbl_slidder/update_action'),
            'id_slidder' => set_value('id_slidder', $row->id_slidder),
            'nama' => set_value('nama', $row->nama),
            'desk' => set_value('desk', $row->desk),
            'foto' => set_value('foto', $row->foto),
            'status' => set_value('status', $row->status),
            'tgl_input' => set_value('tgl_input', $row->tgl_input),
        );
        renderPage('tbl_slidder/tbl_slidder_form', $data);
    } else {
        $this->session->set_flashdata('message', 'Record Not Found');
        redirect(site_url('tbl_slidder'));
    }
}

public function update_action() 
{
    $this->_rules();

    if ($this->form_validation->run() == FALSE) {
        $this->update($this->input->post('id_slidder', TRUE));

    } else {

        $config['upload_path']='./assets/images/slidder';
        $config['allowed_types']='gif|jpg|jpeg|png';
        $config['max_size']='2048';
        $config['max_width']='1500';
        $config['max_height']='1500';
        $this->upload->initialize($config);

        if($this->upload->do_upload('foto')){
            $upload_data=$this->upload->data();
            $nama_file=$upload_data['file_name'];

            $data = array(
              'nama' => $this->input->post('nama',TRUE),
              'desk' => $this->input->post('desk',TRUE),
              'foto' => $nama_file,
              'status' => $this->input->post('status',TRUE),
              'tgl_input' => $this->input->post('tgl_input',TRUE),
          );

            $this->Tbl_slidder_model->update($this->input->post('id_slidder', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('tbl_slidder'));
        }
    }
}

public function delete($id) 
{
    $row = $this->Tbl_slidder_model->get_by_id($id);

    if ($row) {
        $this->Tbl_slidder_model->delete($id);
        $this->session->set_flashdata('message', 'Delete Record Success');
        redirect(site_url('tbl_slidder'));
    } else {
        $this->session->set_flashdata('message', 'Record Not Found');
        redirect(site_url('tbl_slidder'));
    }
}

public function _rules() 
{
 $this->form_validation->set_rules('nama', 'nama', 'trim|required');
 $this->form_validation->set_rules('desk', 'desk', 'trim|required');
       // $this->form_validation->set_rules('foto', 'foto', 'trim|required');
 $this->form_validation->set_rules('status', 'status', 'trim|required');
       // $this->form_validation->set_rules('tgl_input', 'tgl input', 'trim|required');

 $this->form_validation->set_rules('id_slidder', 'id_slidder', 'trim');
 $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
}

}

/* End of file Tbl_slidder.php */
/* Location: ./application/controllers/Tbl_slidder.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-01-09 04:29:17 */
/* http://harviacode.com */