<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tbl_testimoni extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tbl_testimoni_model');
        $this->load->library('form_validation');

        if($this->session->userdata('status') != "login"){
            redirect(base_url("login"));
        }
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'tbl_testimoni/?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'tbl_testimoni/?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'tbl_testimoni/';
            $config['first_url'] = base_url() . 'tbl_testimoni/';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Tbl_testimoni_model->total_rows($q);
        $tbl_testimoni = $this->Tbl_testimoni_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'tbl_testimoni_data' => $tbl_testimoni,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        renderPage('tbl_testimoni/tbl_testimoni_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Tbl_testimoni_model->get_by_id($id);
        if ($row) {
            $data = array(
              'id_testimoni' => $row->id_testimoni,
              'nama_testimoni' => $row->nama_testimoni,
              'testimoni' => $row->testimoni,
              'foto' => $row->foto,
              // 'tgl_input' => $row->tgl_input,
          );
            $this->load->view('tbl_testimoni/tbl_testimoni_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tbl_testimoni'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('tbl_testimoni/create_action'),
            'id_testimoni' => set_value('id_testimoni'),
            'nama_testimoni' => set_value('nama_testimoni'),
            'testimoni' => set_value('testimoni'),
            'foto' => set_value('foto'),
        );
        renderPage('tbl_testimoni/tbl_testimoni_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {

            $config['upload_path']='./assets/images/testimoni';
            $config['allowed_types']='gif|jpg|jpeg|png';
            $config['max_size']='2048';
            $config['max_width']='1500';
            $config['max_height']='1500';
            $this->upload->initialize($config);

            if($this->upload->do_upload('foto')){
                $upload_data=$this->upload->data();
                $nama_file=$upload_data['file_name'];

                $data = array(
                  'nama_testimoni' => $this->input->post('nama_testimoni',TRUE),
                  'testimoni' => $this->input->post('testimoni',TRUE),
                  'foto' => $nama_file,
              );

                $this->Tbl_testimoni_model->insert($data);
                $this->session->set_flashdata('message', 'Create Record Success');
                redirect(site_url('tbl_testimoni'));
            } else {


                $data = array(
                    'button' => 'Create',
                    'action' => site_url('tbl_testimoni/create_action'),
                    'id_testimoni' => set_value('id_testimoni'),
                    'nama_testimoni' => set_value('nama_testimoni'),
                    'testimoni' => set_value('testimoni'),
                    'foto' => set_value('foto'),
                );
                renderPage('tbl_testimoni/tbl_testimoni_form', $data);


            }
        }
    }
    
    public function update($id) 
    {
        $row = $this->Tbl_testimoni_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('tbl_testimoni/update_action'),
                'id_testimoni' => set_value('id_testimoni', $row->id_testimoni),
                'nama_testimoni' => set_value('nama_testimoni', $row->nama_testimoni),
                'testimoni' => set_value('testimoni', $row->testimoni),
                
            );
            renderPage('tbl_testimoni/tbl_testimoni_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tbl_testimoni'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_testimoni', TRUE));
        } else {

            $data = array(
              'nama_testimoni' => $this->input->post('nama_testimoni',TRUE),
              'testimoni' => $this->input->post('testimoni',TRUE),
          );

            $this->Tbl_testimoni_model->update($this->input->post('id_testimoni', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('tbl_testimoni'));
        }
    }


    public function delete($id) 
    {
        $row = $this->Tbl_testimoni_model->get_by_id($id);

        if ($row) {
            $this->Tbl_testimoni_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('tbl_testimoni'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('tbl_testimoni'));
        }
    }

    public function _rules() 
    {
     $this->form_validation->set_rules('nama_testimoni', 'nama testimoni', 'trim|required');
     $this->form_validation->set_rules('testimoni', 'testimoni', 'trim|required');


     $this->form_validation->set_rules('id_testimoni', 'id_testimoni', 'trim');
     $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
 }

}

/* End of file Tbl_testimoni.php */
/* Location: ./application/controllers/Tbl_testimoni.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-01-09 04:54:27 */
/* http://harviacode.com */