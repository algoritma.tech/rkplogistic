<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Tbl_news_model');
		$this->load->model('Tbl_gallery_model');
		$this->load->model('Tbl_kontak_model');
		$this->load->model('Tbl_testimoni_model');
		$this->load->library('form_validation');

	}

	
	public function index()
	{
		$tbl_news = $this->Tbl_news_model->get_limit_6();
		$tbl_gallery = $this->Tbl_gallery_model->get_limit_8();
		$tbl_testimoni = $this->Tbl_testimoni_model->get_limit_3();
		$tbl_testimoni_text = $this->Tbl_testimoni_model->get_limit_3();


		
		$data = array(

			'nourut_testi' => 0,
			'nourut_testi_text' => 0,


			'tbl_testimoni_data' => $tbl_testimoni,
			'tbl_testimoni_text_data' => $tbl_testimoni_text,

			'tbl_news_data' => $tbl_news,
			'tbl_gallery_data' => $tbl_gallery,

			'img_path'      => './assets/captcha/',
			'img_url'       => base_url('assets/captcha'),
			'font_path' 	=> './assets/font/timesbd.ttf',	
			'word_length'   => 4,
			'font_size'     => 20,	
			'img_width'     => 250,
			'img_height'    => 45,
			'expiration'    => 7200,


			'action' => site_url('welcome/kontak_action'),
			'id_kontak' => set_value('id_kontak'),
			'nama' => set_value('nama'),
			'email' => set_value('email'),
			'hp' => set_value('hp'),
			'pesan' => set_value('pesan')

		);

		$captcha = create_captcha($data);

		$sessionCaptcha = $this->session->unset_userdata('captchacode');
		$sessionCaptcha = $this->session->set_userdata('captchacode', $captcha['word']);

		$data['cap_img'] = $captcha['image'];

		$this->load->view('welcome_message', $data);
	}


	public function kontak_action() 
	{
		$inputCaptcha = $this->input->post('captcha');
		$sessionCaptcha = $this->session->userdata('captchacode');

		if( $sessionCaptcha === $inputCaptcha) {

			$this->_rules_kontak();
			if ($this->form_validation->run() == FALSE) {

				redirect(site_url(''));
				$this->session->set_flashdata('message', ' <div class="alert alert-info">aaa.</div>');

			} else {
				$data = array(
					'nama' => $this->input->post('nama',TRUE),
					'email' => $this->input->post('email',TRUE),
					'pesan' => $this->input->post('pesan',TRUE),
					'captcha' => $this->input->post('captcha',TRUE),
				);


				$this->Tbl_kontak_model->insert($data);
				$this->session->set_flashdata('message', ' <div class="alert alert-success" align="center">Pesan Sudah Kami Terima, Kami Akan Menghubungi Anda.</div>');
				redirect(site_url(''.'#contact'));

			}
		}else {
			
			$this->session->set_flashdata('message', '<div class="alert alert-danger" align="center">Input Kode Captcha Salah. Pesan anda tidak terkirim.</div>');
			redirect(site_url(''.'#contact'));
		}
	}


	public function _rules_kontak() 
	{
		$this->form_validation->set_rules('nama', 'nama', 'trim|required');
		$this->form_validation->set_rules('email', 'email', 'trim|required');
		$this->form_validation->set_rules('captcha', 'captcha', 'trim|required');
		$this->form_validation->set_rules('pesan', 'pesan', 'trim|required');

		$this->form_validation->set_rules('id_kontak', 'id_kontak', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}
}
