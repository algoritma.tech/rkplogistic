<div class="about" id="about">
	<div class="container">
		<!-- <h3 class="title">Tentang Kami</h3> -->
		<div class="about-banner-grids ">
			<div class="col-md-6 left-of-about">
				<div class=" about-matter-left">
					<br />
					<br />
					<br />
					
					<span>About</span>
					<h3>
						What is RKP Logistic ?
					</h3>

				</div>
				<!-- <div class="stats-cout agileits w3layouts">
					<div class="col-md-6 col-sm-6 col-xs-6 agileits w3layouts stats-grid stats-grid-1">
						<div class=" agileits-w3layouts counter">3500</div>
						<div class="stat-info-w3ls">
							<h4 class="agileits w3layouts">Klien</h4>
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6 agileits w3layouts stats-grid stats-grid-2">

						<div class=" agileits-w3layouts counter">650</div>
						<div class="stat-info-w3ls">
							<h4 class="agileits w3layouts ">Armada</h4>
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6 stats-grid agileits w3layouts stats-grid-3">

						<div class=" agileits-w3layouts counter">1021</div>
						<div class="stat-info-w3ls">
							<h4 class="agileits w3layouts ">Pengiriman</h4>
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6 stats-grid agileits w3layouts stats-grid-4">

						<div class=" agileits-w3layouts counter">1010</div>
						<div class="stat-info-w3ls">
							<h4 class="agileits w3layouts">Awards</h4>
						</div>
					</div>
					<div class="clearfix"></div>
				</div> -->
				<div class="clearfix"></div>
			</div>
			<!-- <div class="col-md-4 about-pic">

			</div> -->
			<div class="col-md-6 right-of-about">
				<!-- <img src="<?php echo base_url(); ?>assets/images/logo.png" /> -->
					<!-- <div class=" stats-info-right">
						<div class="stats-info-text">
							<h4>5</h4>
							<h5>years of</h5>
							<p>experience</p>
						</div>
					</div> -->

					<div class="about-matter-right">
						<p>
							<strong>PT Rezeki Kempu Putagan (RKP)</strong> mempersembahkan RKP LOGISTIC sebagai perusahaan yang bergerak di bidang transportasi logistik darat, laut dan udara. 

							Berkantor pusat di Medan dan memiliki dua cabang di Jakarta dan Pekanbaru, dengan mengutamakan rute Jawa - Sumatera. 

						</p>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	