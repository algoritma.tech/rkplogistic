<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>SB Admin 2 - Dashboard</title>
  
  <?php $this->load->view('admin/css'); ?>
</head>

<body id="page-top">  
  <div id="wrapper">

    <?php $this->load->view('admin/menu'); ?>
    
    <?php $this->load->view('admin/nav_header'); ?>

    <div id="page-wrapper">
      <div class="row">
        <div class="col-lg-12">

          <div class="card-body">
            

