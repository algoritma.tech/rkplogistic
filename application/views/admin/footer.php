<footer class="sticky-footer bg-white">
	<div class="container my-auto">
		<div class="copyright text-center my-auto">
			<span>Copyright &copy; rkplogistic <?php echo date('Y'); ?> by <a href="https://algotech.co.id/">algotech.co.id</a></span>
		</div>
	</div>
</footer>
