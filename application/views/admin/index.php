<div class="card-body">
	<h1>
		Selamat Datang Admin

	</h1>


	<div class="row" style="margin-bottom: 10px">
		<div class="col-md-4">
		</div>
		<div class="col-md-4 text-center">
			<div style="margin-top: 8px" id="message">
				<?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
			</div>
		</div>
		<div class="col-md-1 text-right">
		</div>
		<div class="col-md-3 text-right">
			<form action="<?php echo site_url('tbl_kontak/index'); ?>" class="form-inline" method="get">
				<div class="input-group">
					<input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
					<span class="input-group-btn">
						<?php 
						if ($q <> '')
						{
							?>
							<a href="<?php echo site_url('tbl_kontak'); ?>" class="btn btn-default">Reset</a>
							<?php
						}
						?>
						<button class="btn btn-primary" type="submit">Search</button>
					</span>
				</div>
			</form>
		</div>
	</div>
	<table class="table table-striped" >

		<tr>
			<td>No</td>
			<td>Nama</td>
			<td>Email</td>
			<td>HP</td>
			<td>Action</td>
		</tr>

		<?php foreach ($tbl_kontak_data as $tbl_kontak) { ?>
			<tr>
				<td><?php echo ++$start; ?></td>
				<td><?php echo $tbl_kontak->nama; ?></td>
				<td><?php echo $tbl_kontak->email; ?></td>
				<td><?php echo $tbl_kontak->hp; ?></td>
				<td>


					<?php 
					echo anchor(site_url('tbl_kontak/read/'.$tbl_kontak->id_kontak),'Read'); 
					echo ' | '; 

					echo anchor(site_url('tbl_kontak/delete/'.$tbl_kontak->id_kontak),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
					?>

				</td>
			</tr>
		<?php } ?>


	</table>


	<div class="row">
		<div class="col-md-6">
			<a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
		</div>
		<div class="col-md-6 text-right">
			<?php echo $pagination ?>
		</div>
	</div>
</div>