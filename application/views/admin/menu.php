<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo base_url(); ?>">
    <div class="sidebar-brand-icon rotate-n-15">
      <i class="fas fa-laugh-wink"></i>
    </div>
    <div class="sidebar-brand-text mx-3">ADMIN RKP Logistic <sup>2</sup></div>
  </a>


  <hr class="sidebar-divider my-0">
  <li class="nav-item active">
    <a class="nav-link" href="<?php echo base_url(); ?>admin">
      <i class="fas fa-fw fa-tachometer-alt"></i>
      <span>Dashboard</span>
    </a>
  </li>

  

  <li class="nav-item active">
    <a class="nav-link" href="<?php echo base_url(); ?>tbl_news">
      <i class="fas fa-fw fa-tachometer-alt"></i>
      <span>News</span>
    </a>
  </li>

  

  <li class="nav-item active">
    <a class="nav-link" href="<?php echo base_url(); ?>tbl_testimoni">
      <i class="fas fa-fw fa-tachometer-alt"></i>
      <span>Testimoni</span>
    </a>
  </li>

   

  <li class="nav-item active">
    <a class="nav-link" href="<?php echo base_url(); ?>tbl_gallery">
      <i class="fas fa-fw fa-tachometer-alt"></i>
      <span>Gallery</span>
    </a>
  </li>

  <li class="nav-item active">
    <a class="nav-link" href="<?php echo base_url(); ?>tbl_kontak">
      <i class="fas fa-fw fa-tachometer-alt"></i>
      <span>Kontak</span>
    </a>
  </li>

  <li class="nav-item active">
    <a class="nav-link" href="<?php echo base_url(); ?>tbl_profile">
      <i class="fas fa-fw fa-tachometer-alt"></i>
      <span>Profile</span>
    </a>
  </li>

  <li class="nav-item active">
    <a class="nav-link" href="<?php echo base_url(); ?>login/logout">
      <i class="fas fa-fw fa-tachometer-alt"></i>
      <span>Logout</span>
    </a>
  </li>

  <hr class="sidebar-divider">


  <div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
  </div>

</ul>

