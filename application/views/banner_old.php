<div class="banner-w3">

	<div class="w3-agile-logo">
		<div class="container">
			<div class=" head-wl">
				<div class="headder-w3">
					<h1><a href="<?php echo base_url(); ?>"></a></h1>
				</div>

				<div class="w3-header-top-right-text">
					<h6 class="caption"> Contact Us</h6>
					<p>0812 5880 088.</p>
				</div>

				<div class="email-right">
					<h6 class="caption">Email Us</h6>
					<p><a href="mailto:rkplogistic@gmail.com" class="info">rkplogistic@gmail.com</a></p>

				</div>


				<div class="agileinfo-social-grids">
					<h6 class="caption">Follow Us</h6>
					<ul>
						<li><a href="#"><span class="fa fa-facebook"></span></a></li>
						<li><a href="#"><span class="fa fa-twitter"></span></a></li>
						<li><a href="#"><span class="fa fa-instagram"></span></a></li>
					</ul>
				</div>

				<div class="clearfix"> </div>
			</div>
		</div>

	</div>
	<div class="top-nav">
		<nav class="navbar navbar-default navbar-fixed-top">

			<!-- //header -->
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>

				<?php $this->load->view('menu'); ?>

			</div>
			<div class="clearfix"> </div>
		</nav>	
	</div>
	<div class="container">
		<!-- header -->
		<header>

			<div class="flexslider-info">
				<section class="slider">
					<div class="flexslider">
						<ul class="slides">
							<li>
								<div class="w3l-info">
									<h4>Selamat Datang Di RKP Logistic</h4>
									<p>Selamat datang di rkp logistic, rkp logistic adalah perusahaan jasa pengiriman baran antar provinsi di indonesia. </p>
									<div class="w3layouts_more-buttn">
										<a href="#" data-toggle="modal" data-target="#myModal">Read More</a>
									</div>
								</div>
							</li>
							<li>
								<div class="w3l-info">
									<h4>Jasa Pengiriman Barang Terpercaya</h4>
									<p>Kami mengirimkan barang anda melalui jalur darat, udara ataupun laut.  </p>
									<div class="w3layouts_more-buttn">
										<a href="#" data-toggle="modal" data-target="#myModal">Read More</a>
									</div>
								</div>
							</li>
							<li>
								<div class="w3l-info">
									<h4>Pengiriman Barang Tepat Waktu</h4>
									<p>Kami memastikan semua barang anda di kirim tepat waktu. </p>
									<div class="w3layouts_more-buttn">
										<a href="#" data-toggle="modal" data-target="#myModal">Read More</a>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</section>
			</div>
		</header>
	</div>
	<div class="clearfix"> </div>
</div>

<div class="modal about-modal fade" id="myModal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Logistical</h4>
			</div>
			<div class="modal-body">
				<div class="out-info">
					<img src="<?php echo base_url(); ?>assets/images/logo.png" alt="" />
					<p>PT. REZEKI KEMPU PUTAGAN (RKP) mempersembahkan RKP LOGISTIC sebagai perusahaan yang bergerak di bidang transportasi logistik darat, laut dan udara. Mengutamakan rute Jawa - Sumatera, PT RKP LOGISTIC berkantor pusat di Medan dan memiliki dua cabang di Jakarta dan Pekanbaru.</p>
				</div>
			</div>
		</div>
	</div>
</div>