<!DOCTYPE html>
<html lang="html">

<head>
	<title>RKP LOGISTIC</title>

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="" />

	<script type="application/x-javascript">
		addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); }
	</script>
	<?php  $this->load->view('css'); ?>

</head>

<body>
	<div class="banner-w3-2">

		<div class="w3-agile-logo">
			<div class="container">
				<div class=" head-wl">
					<div class="headder-w3">
						<h1><a href="<?php echo base_url(); ?>">RKPLOGISTIC</a></h1>
					</div>

					<div class="w3-header-top-right-text">
						<h6 class="caption"> Contact Us</h6>
						<p>0812 5880 088.</p>
					</div>

					<div class="email-right">
						<h6 class="caption">Email Us</h6>
						<p><a href="mailto:rkplogistic@gmail.com" class="info">rkplogistic@gmail.com</a></p>

					</div>


					<div class="agileinfo-social-grids">
						<h6 class="caption">Follow Us</h6>
						<ul>
							<li><a href="#"><span class="fa fa-facebook"></span></a></li>
							<li><a href="#"><span class="fa fa-twitter"></span></a></li>
							<li><a href="#"><span class="fa fa-instagram"></span></a></li>
						</ul>
					</div>

					<div class="clearfix"> </div>
				</div>
			</div>

		</div>
		<div class="top-nav">
			<nav class="navbar navbar-default navbar-fixed-top">

				<!-- //header -->
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>

					<?php $this->load->view('menu'); ?>

				</div>
				<div class="clearfix"> </div>
			</nav>	
		</div>

		<div class="clearfix"> </div>
	</div>



	
	<section class="team" id="team">
		<div class="container">
			<div class="about-head text-center ">
				<h3 class="title">Blog</h3>
			</div>

			<?php foreach ($tbl_news_data as $tbl_news) {  ?>

				<div class="team-grid-top">
					<div class="col-md-4  team1">
						<div class="inner-team1">
							<a href="<?php echo base_url(); ?>blog/read/<?php echo $tbl_news->id_news; ?>"><h3><?php echo $tbl_news->judul; ?></h3>
							</a>

							<a href="<?php echo base_url(); ?>blog/read/<?php echo $tbl_news->id_news; ?>">
								<img src="<?php echo base_url(); ?>assets/images/news/<?php echo $tbl_news->foto; ?>" alt="" />
							</a>

							<div style="color: #fff; text-align: center;">Admin - <?php echo $tbl_news->desk; ?> - <?php echo $tbl_news->tgl_input; ?></div>
						</div>
					</div>

				</div>

			<?php } ?>



		</div>
	</section>


	<?php $this->load->view('layanan'); ?>

	<?php $this->load->view('gallery_welcome'); ?>


	<?php $this->load->view('kontak'); ?>


	<!--//contact-->
	<?php $this->load->view('footer'); ?>

	<?php  $this->load->view('js'); ?>
	<!-- //here ends scrolling icon -->
</body>

</html>