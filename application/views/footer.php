<footer>
	<div class="container">
		<div class="col-md-3 header-side">
			<h2><a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/images/logo-rkp-new.png" width="200" /></a></h2>
		</div>
		<div class="col-md-7 header-side">
			<p>©<?php echo date('Y'); ?> RKP LOGISTIC. All Rights Reserved |  by <a href="http://algotech.co.id/jasa-pembuatan-website-profesional" target="_blank">algotech.co.id</a></p>
		</div>
		<div class="col-md-2 header-side">
			<div class="buttom-social-grids">
				<ul>
					<li><a href="#"><span class="fa fa-facebook"></span></a></li>
					<li><a href="#"><span class="fa fa-twitter"></span></a></li>
					<li><a href="#"><span class="fa fa-instagram"></span></a></li>
				</ul>
			</div>
		</div>
	</div>
</footer>