<!DOCTYPE html>
<html lang="html">

<head>
	<title>Gallery - RKP LOGISTIC</title>

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="" />

	<script type="application/x-javascript">
		addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); }
	</script>
	<?php  $this->load->view('css'); ?>

</head>

<body>
	<?php $this->load->view('banner_2'); ?>


	
	<section class="team" id="team">
		<div class="container">


			<div class="gallery" id="gallery">
				<h3 class="title">Our Gallery</h3>
				<div class="gallery-grids-top">
					<div class="gallery-grids">


						<div class="col-md-12 gallery-right">

							<div class="gallery-right-grid">
								<?php foreach ($tbl_gallery_data as $tbl_gallery) { ?>

								<div class="col-md-3 gallery-grid-img">
									<a class="example-image-link" href="<?php echo base_url(); ?>assets/images/gallery/<?php echo $tbl_gallery->foto; ?>" data-lightbox="example-set" data-title=""><img class="example-image" src="<?php echo base_url(); ?>assets/images/gallery/<?php echo $tbl_gallery->foto; ?>" alt=""></a>
								</div>
								<?php } ?>


							</div>




							<div class="clearfix"> </div>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
			</div>



		</div>
	</section>


	<?php $this->load->view('layanan'); ?>



	<?php $this->load->view('kontak'); ?>



	<!--//contact-->
	<?php $this->load->view('footer'); ?>

	<?php  $this->load->view('js'); ?>
	<!-- //here ends scrolling icon -->
</body>

</html>