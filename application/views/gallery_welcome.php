<br />
<br />
<br />

<p align="center">GALLERY</p>
<h3 class="title">Our Projects</h3>

<section class="center slider" id="gallery">
	<?php foreach ($tbl_gallery_data as $tbl_gallery) { ?>
		<div>
			<img src="<?php echo base_url(); ?>assets/images/gallery/<?php echo $tbl_gallery->foto; ?>" alt="<?php echo $tbl_gallery->foto; ?>" >
		</div>
	<?php } ?>
</section>
