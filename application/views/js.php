<script type='text/javascript' src='<?php echo base_url(); ?>assets/tmp/js/jquery-2.2.3.min.js'></script>
<script src="<?php echo base_url(); ?>assets/tmp/js/bootstrap.js"></script>
<script defer src="<?php echo base_url(); ?>assets/tmp/js/jquery.flexslider.js"></script>
<script type="text/javascript">
	$(window).load(function () {
		$('.flexslider').flexslider({
			animation: "slide",
			start: function (slider) {
				$('body').removeClass('loading');
			}
		});
	});
</script>
<script src="<?php echo base_url(); ?>assets/tmp/js/jquery.waypoints.min.js"></script>
<script src="<?php echo base_url(); ?>assets/tmp/js/jquery.countup.js"></script>
<script>
	$('.counter').countUp();
</script>

<script src="<?php echo base_url(); ?>assets/tmp/js/lightbox-plus-jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/tmp/js/move-top.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/tmp/js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function ($) {
		$(".scroll").click(function (event) {
			event.preventDefault();
			$('html,body').animate({
				scrollTop: $(this.hash).offset().top
			}, 900);
		});
	});
</script>

<script type="text/javascript">
	$(document).ready(function () {

		var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear'
			};


			$().UItoTop({
				easingType: 'easeOutQuart'
			});

		});
	</script>