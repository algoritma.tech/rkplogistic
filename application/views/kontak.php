<div class=" contact-top">
	<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15928.485383361323!2d98.6986513!3d3.5595159!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc59b9bcc745917b6!2sRKP%20LOGISTIC%20%2F%20RKP%20GROUP!5e0!3m2!1sid!2sid!4v1631181395993!5m2!1sid!2sid"></iframe>
</div>

<div class="contact" id="contact">
	<div class="container">
		<h3 class="title ">Hubungi Kami</h3>

		<div style="margin-top: 8px" id="message">
			<?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
		</div>



		<div class="col-md-4">
			<div class="contact-icons">
				<h2 align="center">Head <br /> Office</h2>
				<br />
				<div class="col-md-12 footer_grid_left">
					<div class="col-md-2">
						<div class="icon_grid_left">
							<span class="fa fa-map-marker" aria-hidden="true"></span>
						</div>
					</div>
					<div class="col-md-10">

						<h5>Alamat</h5>
						<p>Jl. Air Bersih No. 49, Kel. Sudi Rejo I, Kec. Medan Kota.<span>Sumatera Utara.</span></p>
					</div>
				</div>

				<div class="col-md-12">
				</div>

				<div class=" col-md-12 footer_grid_left">
					<div class="col-md-2">
						<div class="icon_grid_left">
							<span class="fa fa-volume-control-phone" aria-hidden="true"></span>
						</div>
					</div>
					<div class="col-md-10">

						<h5>Handphone</h5>
						<p>0812 5 880088.<span>.</span></p>
					</div>
				</div>


				<div class=" col-md-12 footer_grid_left">
					<div class="col-md-2">
						<div class="icon_grid_left">
							<span class="fa fa-envelope" aria-hidden="true"></span>
						</div>
					</div>
					<div class="col-md-10">

						<h5>Email</h5>
						<p><span><a href="mailto:rkplogistic@gmail.com">rkplogistic@gmail.com</a></span></p>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-4">

			<div class="contact-icons">

				<h2 align="center">Cabang  <br />Jakarta</h2>
				<br />

				<div class=" col-md-12 footer_grid_left">
					<div class="col-md-2">
						<div class="icon_grid_left">
							<span class="fa fa-map-marker" aria-hidden="true"></span>
						</div>
					</div>
					<div class="col-md-10">
						<h5>Alamat</h5>
						<p>JJl. Ks Tubun No. 6, Kec. Palmera, Jakarta Barat, <span>Jakarta.</span></p>
					</div>
				</div>

				<div class=" col-md-12 footer_grid_left">
					<div class="col-md-2">
						<div class="icon_grid_left">
							<span class="fa fa-volume-control-phone" aria-hidden="true"></span>
						</div>
					</div>

					<div class="col-md-10">
						<h5>Handphone</h5>
						<p>0811 638 9917.<span>.</span></p>
					</div>
				</div>


				<div class=" col-md-12 footer_grid_left">
					<div class="col-md-2">
						<div class="icon_grid_left">
							<span class="fa fa-envelope" aria-hidden="true"></span>
						</div>
					</div>
					<div class="col-md-10">

						<h5>Email</h5>
						<p><span><a href="mailto:rkplogisticjakarta@gmail.com">rkplogisticjakarta@gmail.com</a></span></p>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-4">
			<div class="contact-icons">
				<h2 align="center">
					Cabang <br /> Pekanbaru</h2>
					<br />

					<div class=" col-md-12 footer_grid_left">
						<div class="col-md-2">
							<div class="icon_grid_left">
								<span class="fa fa-map-marker" aria-hidden="true"></span>
							</div>
						</div>
						<div class="col-md-10">
							<h5>Alamat</h5>
							<p>Jl. Nangka Ujung No.168,Kec. Payung Sekaki, Kota Pekanbaru.<span>Riau.</span></p>
						</div>
					</div>

					<div class=" col-md-12 footer_grid_left">
						<div class="col-md-2">
							<div class="icon_grid_left">
								<span class="fa fa-volume-control-phone" aria-hidden="true"></span>
							</div>
						</div>
						<div class="col-md-10">
							<h5>Handphone</h5>
							<p>0811 636 9917.<span>.</span></p>
						</div>
					</div>


					<div class=" col-md-12 footer_grid_left">
						<div class="col-md-2">
							<div class="icon_grid_left">
								<span class="fa fa-envelope" aria-hidden="true"></span>
							</div>
						</div>
						<div class="col-md-10">
							<h5>Email</h5>
							<p><span><a href="mailto:rkplogisticpekanbaru@gmail.com">rkplogisticpekanbaru@gmail.com</a></span></p>
						</div>
					</div>

				</div>
			</div>


		</div>


		<div class="contact-2">
			<div class="col-md-6  col-sm-6 contact-form">
				<h2 class="contact_putih">We are ready to serve you</h2>
				<form action="<?php echo $action; ?>" method="post">
					<div class=" form-right col-md-6">
						<input type="text" name="nama" placeholder="Nama" required="required">
					</div>
					<div class=" form-right col-md-6">
						<input type="email" name="email" placeholder="Email" required="required">
					</div>
					<div class=" form-right col-md-12">

						<textarea name="pesan" placeholder="Pesan" required="required"></textarea>
					</div>
					<div class="form-left col-md-6 ">
						<?php  echo $cap_img; ?>

					</div>
					<div class="form-right col-md-6">
						<input class="phone" type="text" name="captcha" placeholder="Kode Captcha" required="required">
					</div>

					<div class="col-md-12">

						<input type="submit" value="Send Message">
					</div>

				</div>
				<div class="col-md-6 col-sm-6 msg-text" >

					<h3 class="contact_us">CONTACT US</h3>


					<div class="col-md-12 sosmed">
						<img src="<?php echo base_url(); ?>assets/images/logo-ig.png"> rkplogistic
					</div>


					<div class="col-md-12 sosmed">
						<img src="<?php echo base_url(); ?>assets/images/logo-yt.png"> rkplogistic
					</div>

					<div class="clearfix"> </div>
				</div>
			</form>
		</div>

		<div class="clearfix"> </div>



	</div>







