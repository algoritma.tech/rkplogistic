<div class="top-nav" >
	<nav class="navbar navbar-fixed-top" id="menu">

		<!-- //header -->
		<div class="container">
			<div class="navbar-header">

				<a class="navbar-brand" href="#">
					<img width="220" class="logo" src="<?php echo base_url(); ?>assets/images/logo-rkp-new.png">
				</a>



				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>

			<div id="navbar" class="navbar-collapse collapse navbar-right">
				<ul class="nav navbar-nav ">

					<li><a href="<?php echo base_url(); ?>" >Home</a></li>
					<li><a href="<?php echo base_url(); ?>#partner" class="scroll">Our Partner</a></li>
					<li><a href="<?php echo base_url(); ?>#about" class="scroll">About</a></li>
					<li><a href="<?php echo base_url(); ?>#service" class="scroll">Service</a></li>
					<li><a href="<?php echo base_url(); ?>#gallery" class="scroll">Galery</a></li>
					<li><a href="<?php echo base_url(); ?>#contact" class="scroll">Hubungi Kami</a></li>
				</ul>
			</div>

			<div class="clearfix"> </div>
		</nav>	
	</div>