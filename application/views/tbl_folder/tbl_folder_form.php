 <h2 style="margin-top:0px">Tbl_folder <?php echo $button ?></h2>
 <form action="<?php echo $action; ?>" method="post">
    <div class="form-group">
        <label for="varchar">Nama Folder <?php echo form_error('nama_folder') ?></label>
        <input type="text" class="form-control" name="nama_folder" id="nama_folder" placeholder="Nama Folder" value="<?php echo $nama_folder; ?>" />
    </div>
    <div class="form-group">
        <label for="date">Tgl Input <?php echo form_error('tgl_input') ?></label>
        <input type="text" class="form-control" name="tgl_input" id="tgl_input" placeholder="Tgl Input" value="<?php echo date('Y-m-d'); ?>" />
    </div>
    <input type="hidden" name="id_folder" value="<?php echo $id_folder; ?>" /> 
    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
    <a href="<?php echo site_url('tbl_folder') ?>" class="btn btn-default">Cancel</a>
</form>