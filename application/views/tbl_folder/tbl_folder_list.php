
<h2 style="margin-top:0px">Tbl_folder List</h2>
<div class="row" style="margin-bottom: 10px">
    <div class="col-md-4">
        <?php echo anchor(site_url('tbl_folder/create'),'Create', 'class="btn btn-primary"'); ?>
    </div>
    <div class="col-md-4 text-center">
        <div style="margin-top: 8px" id="message">
            <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
        </div>
    </div>
    <div class="col-md-1 text-right">
    </div>
    <div class="col-md-3 text-right">
        <form action="<?php echo site_url('tbl_folder/index'); ?>" class="form-inline" method="get">
            <div class="input-group">
                <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                <span class="input-group-btn">
                    <?php 
                    if ($q <> '')
                    {
                        ?>
                        <a href="<?php echo site_url('tbl_folder'); ?>" class="btn btn-default">Reset</a>
                        <?php
                    }
                    ?>
                    <button class="btn btn-primary" type="submit">Search</button>
                </span>
            </div>
        </form>
    </div>
</div>
<table class="table table-bordered" style="margin-bottom: 10px">
    <tr>
        <th>No</th>
        <th>Nama Folder</th>
        <th>Tgl Input</th>
        <th>Action</th>
        </tr><?php
        foreach ($tbl_folder_data as $tbl_folder)
        {
            ?>
            <tr>
                <td width="80px"><?php echo ++$start ?></td>
                <td><?php echo $tbl_folder->nama_folder ?></td>
                <td><?php echo $tbl_folder->tgl_input ?></td>
                <td style="text-align:center" width="200px">
                    <?php 
                    echo anchor(site_url('tbl_folder/read/'.$tbl_folder->id_folder),'Read'); 
                    echo ' | '; 
                    echo anchor(site_url('tbl_folder/update/'.$tbl_folder->id_folder),'Update'); 
                    echo ' | '; 
                    echo anchor(site_url('tbl_folder/delete/'.$tbl_folder->id_folder),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
                    ?>
                </td>
            </tr>
            <?php
        }
        ?>
    </table>
    <div class="row">
        <div class="col-md-6">
            <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
        </div>
        <div class="col-md-6 text-right">
            <?php echo $pagination ?>
        </div>
    </div>