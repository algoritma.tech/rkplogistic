<h2 style="margin-top:0px">Kategori Produk <?php echo $button ?></h2>
<form action="<?php echo $action; ?>" method="post">
   <div class="form-group">
    <label for="varchar">Nama Kategori Produk <?php echo form_error('nama_kategori_produk') ?></label>
    <input type="text" class="form-control" name="nama_kategori_produk" id="nama_kategori_produk" placeholder="Nama Kategori Produk" value="<?php echo $nama_kategori_produk; ?>" />
</div>
<input type="hidden" name="id_kategori_produk" value="<?php echo $id_kategori_produk; ?>" /> 
<button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
<a href="<?php echo site_url('tbl_kategori_produk') ?>" class="btn btn-default">Cancel</a>
</form>