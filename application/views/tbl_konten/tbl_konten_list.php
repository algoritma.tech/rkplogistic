<h2 style="margin-top:0px">Konten List</h2>
<div class="row" style="margin-bottom: 10px">

    <div class="col-md-12 text-center">
        <div style="margin-top: 8px" id="message">
            <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
        </div>
    </div>
    
    <div class="col-md-4">
        <?php echo anchor(site_url('tbl_konten/create'),'Create', 'class="btn btn-primary"'); ?>
    </div>
    <div class="col-md-4 text-center">
        <div style="margin-top: 8px" id="message">
            <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
        </div>
    </div>
    <div class="col-md-1 text-right">
    </div>
    <div class="col-md-3 text-right">
        <form action="<?php echo site_url('tbl_konten/index'); ?>" class="form-inline" method="get">
            <div class="input-group">
                <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                <span class="input-group-btn">
                    <?php 
                    if ($q <> '')
                    {
                        ?>
                        <a href="<?php echo site_url('tbl_konten'); ?>" class="btn btn-default">Reset</a>
                        <?php
                    }
                    ?>
                    <button class="btn btn-primary" type="submit">Search</button>
                </span>
            </div>
        </form>
    </div>
</div>
<table class="table table-bordered" style="margin-bottom: 10px">
    <tr>
        <th>No</th>
        <th>Nama Konten</th>
        <th>Desk</th>
        <th>Foto</th>
        <th>Tgl Input</th>
        <th>Action</th>
        </tr><?php foreach ($tbl_konten_data as $tbl_konten) { ?>
            <tr>
               <td width="80px"><?php echo ++$start ?></td>
               <td><?php echo $tbl_konten->nama_konten ?></td>
               <td><?php echo $tbl_konten->desk ?></td>
               <td><a href="<?php echo base_url(); ?>tbl_konten/update_foto/<?php echo $tbl_konten->id_konten; ?>"><img src="<?php echo base_url()."assets/images/konten/".$tbl_konten->foto ?>" height="100" width="100"></a></td>
               <td><?php echo $tbl_konten->tgl_input ?></td>
               <td style="text-align:center">
                <?php 
                    // echo anchor(site_url('tbl_konten/read/'.$tbl_konten->id_konten),'Read'); 
                    // echo ' | '; 
                echo anchor(site_url('tbl_konten/update/'.$tbl_konten->id_konten),'Update'); 
                echo ' | '; 
                echo anchor(site_url('tbl_konten/delete/'.$tbl_konten->id_konten),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
                ?>
            </td>
        </tr>
    <?php } ?>
</table>
<div class="row">
    <div class="col-md-6">
        <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
    </div>
    <div class="col-md-6 text-right">
        <?php echo $pagination ?>
    </div>
</div>
