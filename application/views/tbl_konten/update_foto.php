<h2 style="margin-top:0px">Konten <?php echo $button ?></h2>
<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
	<div class="row">

		<div class="form-group col-md-4">
			<label for="varchar">Nama Konten <?php echo form_error('nama_konten') ?></label>
			<input type="text" disabled="disabled" class="form-control" name="nama_konten" id="nama_konten" placeholder="Nama Konten" value="<?php echo $nama_konten; ?>" />
		</div>

		<div class="form-group col-md-4">
			<label for="varchar">Desk <?php echo form_error('desk') ?></label>
			<textarea disabled="disabled"  class="form-control" name="desk" id="desk" placeholder="Desk"><?php echo $desk; ?></textarea>
		</div>

		<div class="form-group col-md-4">
			<label for="konten">Konten <?php echo form_error('konten') ?></label>
			<img width="100%" src="<?php echo base_url(); ?>assets/images/konten/<?php echo $foto; ?>">>
		</div>
	</div>

	<div class="form-group col-md-12">

		<input type="file" name="foto" id="foto" required="required" value="<?php echo $foto; ?>" />
	</div>

	<div class="form-group col-md-12">

		<input type="hidden" name="id_konten" value="<?php echo $id_konten; ?>" /> 
		<button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
		<a href="<?php echo site_url('tbl_konten') ?>" class="btn btn-default">Cancel</a>
	</div>
</form>
