<h2 style="margin-top:0px">News <?php echo $button ?></h2>
<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
 <div class="col-md-12 text-center">
    <div style="margin-top: 8px" id="message">
        <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
    </div>
</div>
<div class="form-group">
    <label for="varchar">Judul <?php echo form_error('judul') ?></label>
    <input type="text" class="form-control" name="judul" id="judul" placeholder="Judul" value="<?php echo $judul; ?>" />
</div>
<div class="form-group">
    <label for="varchar">Desk <?php echo form_error('desk') ?></label>
    <textarea class="form-control" name="desk" id="desk" ><?php echo $desk; ?></textarea>
</div>

<?php if($button == 'Create') { ?>
    <div class="form-group">
        <label for="foto">Foto <?php echo form_error('foto') ?></label><br>
        <input type="file" name="foto" id="foto" value="<?php echo $foto; ?>" />
    </div>
<?php } ?>

<div class="form-group">
    <label for="konten">Konten <?php echo form_error('konten') ?></label>
    <textarea class="ckeditor" rows="3" name="konten" id="ckeditor" placeholder="Konten"><?php echo $konten; ?></textarea>
</div>

<input type="hidden" name="id_news" value="<?php echo $id_news; ?>" /> 
<button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
<a href="<?php echo site_url('tbl_news') ?>" class="btn btn-default">Cancel</a>
</form>