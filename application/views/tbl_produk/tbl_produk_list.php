<div class="card-body">
    <h2 style="margin-top:0px">Produk List</h2>
    <div class="row" style="margin-bottom: 10px">
        <div class="col-md-4">
            <?php echo anchor(site_url('tbl_produk/create'),'Create', 'class="btn btn-primary"'); ?>
        </div>
        <div class="col-md-4 text-center">
            <div style="margin-top: 8px" id="message">
                <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
            </div>
        </div>
        <div class="col-md-1 text-right">
        </div>
        <div class="col-md-3 text-right">
            <form action="<?php echo site_url('tbl_produk/index'); ?>" class="form-inline" method="get">
                <div class="input-group">
                    <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                    <span class="input-group-btn">
                        <?php 
                        if ($q <> '')
                        {
                            ?>
                            <a href="<?php echo site_url('tbl_produk'); ?>" class="btn btn-default">Reset</a>
                            <?php
                        }
                        ?>
                        <button class="btn btn-primary" type="submit">Search</button>
                    </span>
                </div>
            </form>
        </div>
    </div>
    <table class="table table-bordered" style="margin-bottom: 10px">
        <tr>
            <th>No</th>
            <th>Kategori Produk</th>
            <th>Nama Produk</th>
            <th>Foto1</th>
            <th>Foto2</th>
            <th>Foto3</th>
            <th>Desk</th>
            <th>Harga</th>
            <th>Berat</th>
            <th>Task</th>
            <!-- <th>Tgl Input</th> -->
            <th>Action</th>
            </tr><?php
            foreach ($tbl_produk_data as $tbl_produk)
            {
                ?>
                <tr>
                   <td width="80px"><?php echo ++$start ?></td>
                   <td><?php echo $tbl_produk->id_kategori_produk ?></td>
                   <td><?php echo $tbl_produk->nama_produk ?></td>
                   <td><img src="<?php echo base_url()."assets/images/produk/".$tbl_produk->foto1 ?>" height="100" width="100"></td>
                   <td><img src="<?php echo base_url()."assets/images/produk/".$tbl_produk->foto2 ?>" height="100" width="100"></td>
                   <td><img src="<?php echo base_url()."assets/images/produk/".$tbl_produk->foto2 ?>" height="100" width="100"></td>
                   <td><?php echo $tbl_produk->desk ?></td>
                   <td><?php echo $tbl_produk->harga ?></td>
                   <td><?php echo $tbl_produk->berat ?></td>
                   <td><?php echo $tbl_produk->task ?></td>
                   <!-- <td><?php echo $tbl_produk->tgl_input ?></td> -->
                   <td style="text-align:center" >
                    <?php 
                    // echo anchor(site_url('tbl_produk/read/'.$tbl_produk->id_produk),'Read'); 
                    // echo ' | '; 
                    echo anchor(site_url('tbl_produk/update/'.$tbl_produk->id_produk),'Update'); 
                    echo ' | '; 
                    echo anchor(site_url('tbl_produk/delete/'.$tbl_produk->id_produk),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
                    ?>
                </td>
            </tr>
            <?php
        }
        ?>
    </table>
    <div class="row">
        <div class="col-md-6">
            <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
        </div>
        <div class="col-md-6 text-right">
            <?php echo $pagination ?>
        </div>
    </div>
</div>