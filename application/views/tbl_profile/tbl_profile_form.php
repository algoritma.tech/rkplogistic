<div class="card-body">
    <h2 style="margin-top:0px">Profile <?php echo $button ?></h2>
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">

        <div class="col-md-12 text-center">
            <div style="margin-top: 8px" id="message">
                <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
            </div>
        </div>

        <div class="form-group">
            <label for="varchar">Nama Usaha <?php echo form_error('nama_usaha') ?></label>
            <input type="text" class="form-control" name="nama_usaha" id="nama_usaha" placeholder="Nama Usaha" value="<?php echo $nama_usaha; ?>" />
        </div>

        <?php if($button == 'Create') { ?>
            <div class="form-group">
                <label for="logo">Logo <?php echo form_error('logo') ?></label><br>
                <input type="file" name="logo" id="logo" value="<?php echo $logo; ?>" required="required" />
            </div>
        <?php } ?>

        <div class="form-group">
            <label for="varchar">Alamat <?php echo form_error('alamat') ?></label>
            <input type="text" class="form-control" name="alamat" id="alamat" placeholder="Alamat" value="<?php echo $alamat; ?>" />
        </div>
        <div class="form-group">
            <label for="maps">Maps <?php echo form_error('maps') ?></label>
            <textarea class="form-control" rows="3" name="maps" id="maps" placeholder="Maps"><?php echo $maps; ?></textarea>
        </div>
        <div class="form-group">
            <label for="url_fb">Url Fb <?php echo form_error('url_fb') ?></label>
            <textarea class="form-control" rows="3" name="url_fb" id="url_fb" placeholder="Url Fb"><?php echo $url_fb; ?></textarea>
        </div>
        <div class="form-group">
            <label for="url_ig">Url Ig <?php echo form_error('url_ig') ?></label>
            <textarea class="form-control" rows="3" name="url_ig" id="url_ig" placeholder="Url Ig"><?php echo $url_ig; ?></textarea>
        </div>
        <div class="form-group">
            <label for="url_tw">Url Tw <?php echo form_error('url_tw') ?></label>
            <textarea class="form-control" rows="3" name="url_tw" id="url_tw" placeholder="Url Tw"><?php echo $url_tw; ?></textarea>
        </div>
        <div class="form-group">
            <label for="int">No Telp <?php echo form_error('no_telp') ?></label>
            <input type="text" class="form-control" name="no_telp" id="no_telp" placeholder="No Telp" value="<?php echo $no_telp; ?>" />
        </div>
        <div class="form-group">
            <label for="int">No Hp <?php echo form_error('no_hp') ?></label>
            <input type="text" class="form-control" name="no_hp" id="no_hp" placeholder="No Hp" value="<?php echo $no_hp; ?>" />
        </div>
        <div class="form-group">
            <label for="int">No Wa <?php echo form_error('no_wa') ?></label>
            <input type="text" class="form-control" name="no_wa" id="no_wa" placeholder="No Wa" value="<?php echo $no_wa; ?>" />
        </div>
        <input type="hidden" name="id_profile" value="<?php echo $id_profile; ?>" /> 
        <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
        <a href="<?php echo site_url('tbl_profile') ?>" class="btn btn-default">Cancel</a>
    </form>
</div>