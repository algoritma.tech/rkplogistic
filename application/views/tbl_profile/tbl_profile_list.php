<div class="card-body">
    <h2 style="margin-top:0px">Profile List</h2>
    <div class="row" style="margin-bottom: 10px">
        <div class="col-md-4">
            
        </div>
        <div class="col-md-4 text-center">
            <div style="margin-top: 8px" id="message">
                <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
            </div>
        </div>
        <div class="col-md-1 text-right">
        </div>
        <div class="col-md-3 text-right">
            <form action="<?php echo site_url('tbl_profile/index'); ?>" class="form-inline" method="get">
                <div class="input-group">
                    <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                    <span class="input-group-btn">
                        <?php 
                        if ($q <> '')
                        {
                            ?>
                            <a href="<?php echo site_url('tbl_profile'); ?>" class="btn btn-default">Reset</a>
                            <?php
                        }
                        ?>
                        <button class="btn btn-primary" type="submit">Search</button>
                    </span>
                </div>
            </form>
        </div>
    </div>
    <table class="table table-bordered" style="margin-bottom: 10px">
        <tr>
            <th>No</th>
            <th>Nama Usaha</th>
            <th>Logo</th>
            <th>Alamat</th>
            <th>Maps</th>
            <th>Url Fb</th>
            <th>Url Ig</th>
            <th>Url Tw</th>
            <th>No Telp</th>
            <th>No Hp</th>
            <th>No Wa</th>
            <th>Action</th>
            </tr><?php
            foreach ($tbl_profile_data as $tbl_profile)
            {
                ?>
                <tr>
                   <td width="80px"><?php echo ++$start ?></td>
                   <td><?php echo $tbl_profile->nama_usaha ?></td>
                   <td><img src="<?php echo base_url()."assets/images/profile/".$tbl_profile->logo ?>" height="100" width="100"></td>
                   <td><?php echo $tbl_profile->alamat ?></td>
                   <td><?php echo $tbl_profile->maps ?></td>
                   <td><?php echo $tbl_profile->url_fb ?></td>
                   <td><?php echo $tbl_profile->url_ig ?></td>
                   <td><?php echo $tbl_profile->url_tw ?></td>
                   <td><?php echo $tbl_profile->no_telp ?></td>
                   <td><?php echo $tbl_profile->no_hp ?></td>
                   <td><?php echo $tbl_profile->no_wa ?></td>
                   <td style="text-align:center" width="200px">
                    <?php 
				// echo anchor(site_url('tbl_profile/read/'.$tbl_profile->id_profile),'Read'); 
				// echo ' | '; 
                    echo anchor(site_url('tbl_profile/update/'.$tbl_profile->id_profile),'Update'); 
				// echo ' | '; 
				// echo anchor(site_url('tbl_profile/delete/'.$tbl_profile->id_profile),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
                    ?>
                </td>
            </tr>
            <?php
        }
        ?>
    </table>
    <div class="row">
        <div class="col-md-6">
            <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
        </div>
        <div class="col-md-6 text-right">
            <?php echo $pagination ?>
        </div>
    </div>
</div>