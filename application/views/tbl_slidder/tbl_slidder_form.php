<h2 style="margin-top:0px">Slidder <?php echo $button ?></h2>
<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
    
    <div class="col-md-12 text-center">
        <div style="margin-top: 8px" id="message">
            <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-md-4">
            <label for="varchar">Nama <?php echo form_error('nama') ?></label>
            <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama" value="<?php echo $nama; ?>" />
        </div>
        <div class="form-group col-md-4">
            <label for="desk">Desk <?php echo form_error('desk') ?></label>
            <textarea class="form-control" rows="3" name="desk" id="desk" placeholder="Desk"><?php echo $desk; ?></textarea>
        </div>


        <div class="form-group col-md-4">
            <label for="varchar">Status <?php echo form_error('status') ?></label>
            <select name="status" id="status" class="form-control">
                <option value="tampil" <?php if($status == 'tampil') { echo 'selected="selected"';} ?> >Tampil</option>
                <option value="tidak" <?php if($status == 'tidak') { echo 'selected="selected"';} ?> >Tidak Tampil</option>

            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="foto">Foto  ( Ukuran 1000px x 750px )<?php echo form_error('foto') ?></label><br>
        <input type="file" name="foto" id="foto" value="<?php echo $foto; ?>" />
    </div>


    <input type="hidden" name="id_slidder" value="<?php echo $id_slidder; ?>" /> 
    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
    <a href="<?php echo site_url('tbl_slidder') ?>" class="btn btn-default">Cancel</a>
</form>
