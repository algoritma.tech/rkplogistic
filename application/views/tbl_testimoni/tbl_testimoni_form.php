<h2 style="margin-top:0px">Testimoni <?php echo $button ?></h2>
<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="form-group col-md-6">
            <label for="varchar">Nama Testimoni <?php echo form_error('nama_testimoni') ?></label>
            <input type="text" class="form-control" name="nama_testimoni" id="nama_testimoni" placeholder="Nama Testimoni" value="<?php echo $nama_testimoni; ?>" />
        </div>
        <div class="form-group col-md-6">
            <label for="varchar">Testimoni <?php echo form_error('testimoni') ?></label>
            <textarea class="form-control" name="testimoni" id="testimoni" placeholder="Testimoni"><?php echo $testimoni; ?></textarea>
        </div>
    </div>
    
    <?php if($button == 'Create') { ?>
        <div class="form-group">
            <label for="foto">Foto ( Ukuran 500px x 500px )<?php echo form_error('foto') ?></label><br>
            <input type="file" name="foto" id="foto" value="<?php echo $foto; ?>" />
        </div>
    <?php } ?>

    <input type="hidden" name="id_testimoni" value="<?php echo $id_testimoni; ?>" /> 
    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
    <a href="<?php echo site_url('tbl_testimoni') ?>" class="btn btn-default">Cancel</a>
</form>