<div class="testimonials" id="testimonials">
	<div class="container">

		<h3 class="title">testimonials</h3>

		<div class="carousel slide" data-ride="carousel" id="quote-carousel">
			<!-- Bottom Carousel Indicators -->
			<ol class="carousel-indicators">
				<?php foreach ($tbl_testimoni_data as $tbl_testimoni) { ?>
					<?php $nourut = ++$nourut_testi; ?>
					<li data-target="#quote-carousel" class="<?php if($nourut == 1) { echo 'active';} ?>" data-slide-to="<?php echo $nourut-1 ?>" 

						><img class="img-responsive " src="<?php echo base_url(); ?>assets/images/testimoni/<?php echo $tbl_testimoni->foto; ?>" alt="">
					</li>
				<?php }   ?>
			</ol>

			<div class="carousel-inner text-center">



				<?php foreach ($tbl_testimoni_text_data as $tbl_testimoni_text) { ?>


					<div class="item <?php if(++$nourut_testi_text == 1) { echo 'active'; } ?>">
						<blockquote>
							<div class="row">
								<div class=" left-matter">
									<p><?php echo $tbl_testimoni_text->testimoni ?></p>
									<h5>--<?php echo $tbl_testimoni_text->nama_testimoni; ?></h5>
								</div>


							</div>
						</blockquote>
					</div>

				<?php }  ?>


			</div>

					<!-- Carousel Buttons Next/Prev 
					<a data-slide="prev" href="#quote-carousel" class="left carousel-control"><span class="fa fa-chevron-left"></span></a>
					<a data-slide="next" href="#quote-carousel" class="right carousel-control"><span class="fa fa-chevron-right"></span></a> -->
				</div>
			</div>
		</div>


		<!-- 
						<?php foreach ($tbl_testimoni_data as $tbl_testimoni) { ?>
							<li data-target="#quote-carousel" data-slide-to="0" class="active"><img class="img-responsive " src="<?php echo base_url(); ?>assets/tmp/images/t11.jpg" alt="">
							</li>
							<?php //} ?>



						</ol>

						<div class="carousel-inner text-center">
							<?php // foreach ($tbl_testimoni_text_data as $tbl_testimoni_text) { ?>

								<div class="item active">
									<blockquote>
										<div class="row">
											<div class="left-matter">
												<p><?php echo  $tbl_testimoni->testimoni ?></p>
												<h5>--<?php echo $tbl_testimoni->nama_testimoni; ?></h5>
											</div>


										</div>
									</blockquote>
								</div>
							<?php } ?> -->