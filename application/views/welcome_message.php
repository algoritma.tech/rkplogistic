<!DOCTYPE html>
<html lang="html">

<head>
	<title>RKP LOGISTIC</title>

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="" />
  <meta name="description" content="" />


  <script type="application/x-javascript">
    addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
    function hideURLbar(){ window.scrollTo(0,1); }
  </script>

  <?php  $this->load->view('css'); ?>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"   crossorigin="anonymous">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"   crossorigin="anonymous">

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"   crossorigin="anonymous"></script>

  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/gallery_jquery/slick/slick.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/gallery_jquery/slick/slick-theme.css">



</head>

<body>


	<?php $this->load->view('menu'); ?>

	<?php $this->load->view('banner'); ?>

	<?php $this->load->view('about_welcome'); ?>

	<?php $this->load->view('layanan'); ?>

	<?php //$this->load->view('blog_welcome'); ?>

	<?php $this->load->view('gallery_welcome'); ?>

	<?php //$this->load->view('testimoni'); ?>

	<?php $this->load->view('kontak'); ?>

	<?php $this->load->view('footer'); ?>

	<?php  $this->load->view('js'); ?>

	<!-- //here ends scrolling icon -->


  <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/gallery_jquery/slick/slick.js" type="text/javascript" charset="utf-8"></script>


  <script type="text/javascript">
    $(document).on('ready', function() {

     $(".center").slick({
      dots: true,
      infinite: true,
      centerMode: true,
      slidesToShow: 3,
      slidesToScroll: 3
    });

   });
 </script> 

</body>

</html>